$(document).ready(function () {
  AOS.init({
    duration: 500,
    disable: false,
    easing: 'easeOutExpo'
  });
  $('.review-slide').createSlider(1, 1);
  $(".review-slide").on("swipe", function (){
    var linkactive = $('.review-slide__item.slick-active').attr('data-img');
    $('.review-img').css('background-image', 'url(' + linkactive + ')');
  });
  $('.countdown').countdown((new Date().getTime()) + 1000 * 60 * 60 * 24 * 3)
  $('.prd-slide').createSlider(4, 2, false)
  $('.feedback').createSlider(4, 1, true)
  $('.menu__btn').toogleElement('.menu__nav')

  $('*[data-modal]').click(function () {
    var modalOpen = $($(this).data('modal'));
    modalOpen.addClass('open');
  });

  $('*[data-dismiss]').click(function () {
    var modalClose = $($(this).data('dismiss'));
    modalClose.removeClass('open');
  });

  $('.modal-close, .modal').click(function () {
    $('.modal').removeClass('open');
  })

  $('.modal-box').click(function () {
    event.stopPropagation();
  })
  $('.category-button').click(function () {
    $('.category-menu').addClass('open')
  })
  $('.category-menu-close').click(function () {
    $('.category-menu').removeClass('open')
  })
})

jQuery.fn.extend({
  scrollTo: function (element) {
    var element = $(element);
    $(this).click(function () {
      $(this).parents().find('.menu__tabs a').removeClass('active');
      $(this).find('a').addClass('active');

      $('html').animate({
        scrollTop: element.offset().top - 100
      }, 1000)
    })
  },

  countdown: function (timestamp) {
    var e = $(this);
    var now = new Date().getTime();
    var x = setInterval(function () {
      now = now + 1000;
      // Find the distance between now and the count down date
      var distance = timestamp - now;

      // Time calculations for days, hours, minutes and seconds
      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);
      if (seconds < 0 && minutes < 0 && hours < 0 && days < 0) {
        days = hours = minutes = seconds = "00"
        clearInterval(x);
      }
      if (days.toString().length == 1) {
        days = "0" + days;
      }
      if (hours.toString().length == 1) {
        hours = "0" + hours;
      }
      if (minutes.toString().length == 1) {
        minutes = "0" + minutes;
      }
      if (seconds.toString().length == 1) {
        seconds = "0" + seconds;
      }
      e.find('.dd').html(days);
      e.find('.hh').html(hours);
      e.find('.mm').html(minutes);
      e.find('.ss').html(seconds);

    }, 1000);
  },

  addAnimate: function (array) {
    var wh = $(window).height();
    var add_class_name = "animated fadeInUp";
    $(window).scroll(function () {
      var scroll = $(window).scrollTop();
      for (var element in array) {
        add_class_name = "animated " + array[element];
        if (scroll > $(element).position().top - wh) {
          $(element).addClass(add_class_name);
        }
      }
    });
  },

  elementFixScroll: function (class_name) {
    var e = $(this);
    var position = e.position().top;
    var offsetTop = e.offset().top;
    var scroll_h = $(window).scrollTop();
    if (scroll_h >= position && offsetTop != 0) {
      e.addClass(class_name);
    }

    $(window).scroll(function () {
      scroll_h = $(window).scrollTop();
      if (scroll_h > position) {
        e.addClass(class_name);
      } else {
        e.removeClass(class_name);
      }
    })
  },

  toogleElement: function (element) {
    var e = $(element)
    $(this).click(function () {
      e.slideToggle()
    })
  },

  createSlider: function (itemShow, itemShowMb, autoWidth) {
    $(this).slick({
      infinite: true,
      slidesToShow: itemShow,
      slidesToScroll: 1,
      dots: true,
      variableWidth: autoWidth,
      prevArrow: '<button type="button" class="btn btn--white btn--circle btn--shadow slick-prev"><i class="yinicon-arrow-left"></i></button>',
      nextArrow: '<button type="button" class="btn btn--white btn--circle btn--shadow slick-next"><i class="yinicon-arrow-right"></i></button>',
      responsive: [
        {
          breakpoint: 960,
          settings: {
            slidesToShow: (itemShow <= 1 ? 1 : itemShow - 1),
            slidesToScroll: itemShowMb,
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: (itemShow <= 1 ? 1 : itemShow - 1),
            slidesToScroll: itemShowMb,
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: itemShowMb,
            slidesToScroll: itemShowMb
          }
        }
      ]
    });
  }
})



