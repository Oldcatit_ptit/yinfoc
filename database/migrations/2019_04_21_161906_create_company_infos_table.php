<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyInfosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    if (!Schema::hasTable('company_infos')) {
      Schema::create('company_infos', function (Blueprint $table) {
        $table->increments('id');
        $table->string('mail_support')->nullable();
        $table->string('image')->nullable();
        $table->string('mail_order')->nullable();
        $table->string('mail_recruitment')->nullable();
        $table->string('phone')->nullable();
        $table->string('phone_tech')->nullable();
        $table->string('address')->nullable();
        $table->string('facebook')->nullable();
        $table->string('google')->nullable();
        $table->string('instagram')->nullable();
        $table->string('youtube')->nullable();
        $table->string('behance')->nullable();
        $table->string('dribbble')->nullable();
        $table->string('name')->nullable();
        $table->text('intro')->nullable();
        $table->text('intro_title')->nullable();
        $table->timestamps();
      });

      // \Illuminate\Support\Facades\DB::table('company_infos')->insert([
      //   'name' => 'yinfoc'
      // ]);
    }
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('company_infos');
  }
}
