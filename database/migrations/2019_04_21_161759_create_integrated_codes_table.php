<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIntegratedCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('integrated_codes', function (Blueprint $table) {
            $table->increments('id');
            $table->text('chatbot')->nullable();
            $table->text('facebook')->nullable();
            $table->text('google')->nullable();
            $table->text('youtube')->nullable();
            $table->text('other')->nullable();
            $table->timestamps();
        });

      \Illuminate\Support\Facades\DB::table('integrated_codes')->insert([
        'other' => 'yinfoc'
      ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('integrated_codes');
    }
}
