<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    if (!Schema::hasTable('products')) {
      Schema::create('products', function (Blueprint $table) {
        $table->increments('id');
        $table->string('name')->nullable();
        $table->text('des')->nullable();
        $table->string('image')->nullable();
        $table->string('price')->nullable();
        $table->string('price_sale')->nullable();
        $table->string('url_demo')->nullable();
        $table->string('slug')->unique();
        $table->text('functions')->nullable();
        $table->timestamps();
      });
    }
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('products');
  }
}
