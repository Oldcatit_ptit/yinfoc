<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedbackTable extends Migration
{
    public function up()
    {
      if(!Schema::hasTable('feedback')){
        Schema::create('feedback', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image')->nullable();
            $table->integer('rank')->nullable();
            $table->string('name');
            $table->string('name_sub')->nullable();
            $table->text('content');
            $table->boolean('status')->default(1);
        });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedback');
    }
}
