<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

use Illuminate\Support\Facades\Route;
Route::get('/clear-cache', function () {
    Artisan::call('config:cache');
    Artisan::call('cache:clear');
    /*Artisan::call('cache:table');*/
    /*Artisan::call('migrate');*/
    Cache::flush();
    var_dump('config cache successed');
});
Route::get('/login-admin', 'LoginController@getLogin')->name('admin.login');
Route::post('/login-admin', 'LoginController@postLogin')->name('admin.post.login');
Route::get('/dang-xuat.html', 'LoginController@getLogout')->name('admin.logout');

Auth::routes();
Route::group(['middleware' => ['admin'], 'prefix' => 'yinadmin'], function () {
    Route::get('/', 'ProductController@index')->name('admin-home');

    Route::prefix('order')->group(function () {
        Route::get('product', 'OrderController@products');
        Route::get('service', 'OrderController@service');
    });

    Route::resources([
        'products'           => 'ProductController',
        'product-categories' => 'ProductCategoryController',
        'options'            => 'OptionController',
        'order'              => 'OrderController',
        'services'           => 'ServiceController',
        'feedback'           => 'FeedbackController',
        'company-info'       => 'CompanyInfoController',
        'integratedCode'     => 'IntegratedCodeController',
        'components'         => 'ComponentController',
    ]);

    Route::prefix('component')->group(function () {
        Route::get('products', 'ComponentController@getFeaturedProducts')->name('component-product');
        Route::get('banners', 'ComponentController@getBanners')->name('component-banner');
    });
});

Route::get('product', function () {
    return view('admin.index');
});

// Client route
Route::get('/', 'Client\HomeController@index')->name('home');
Route::get('kho-giao-dien/{slug}', 'ProductCategoryController@show')->name('product-category-view');
Route::get('kho-giao-dien', 'ProductCategoryController@viewAll')->name('product-category-all');
Route::get('lien-he', function () {
    return view('client.page.contact');
});

Route::get('blog', function () {
    return view('client.page.blog');
});
Route::get('gioi-thieu', function () {
    return view('client.page.intro');
});
Route::get('dich-vu', 'Client\ServiceController@index')->name('service-view');
Route::get('gioi-thieu', 'Client\ClientController@showIntro')->name('intro-view');
Route::get('trai-nghiem-landing-page/{slug}', 'Client\ProductController@demo')->name('product-demo');
Route::get('landing-page/{slug}', 'Client\ProductController@index')->name('product-view');
Route::post('clientCreateOrder', 'Client\OrderController@clientCreateOrder');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
