<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
  <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600,700,800&amp;subset=vietnamese"
        rel="stylesheet">
  <style>
    :root {
      font-size: 10px;
    }

    body {
      font-family: 'Nunito', sans-serif;
      font-size: 1.4rem;
      background-color: #eeeeee;
      padding-top: 1rem;
      padding-bottom: 1rem;
    }

    .btn-orange {
      border-radius: 0.5rem;
      border: none;
      cursor: pointer;
      display: inline-flex;
      justify-content: center;
      align-items: center;
      text-align: center;
      font-weight: 700;
      background: linear-gradient(180deg, #FF9324 0%, #FF7810 100%);
      color: #ffffff !important;
      padding: 0.5em 2em;
      font-size: 1.4rem;
      text-decoration: none;
    }

    .text-center {
      text-align: center;
    }

    .text-orange {
      color: #FF9D12;
    }

    .text-white {
      color: #ffffff !important;
    }

    .p-1 {
      padding: 1rem;
    }

    .py-2 {
      padding-top: 1.5rem;
      padding-bottom: 1.5rem;
    }

    .bg-blue-dark {
      background-color: #000D37;
      color: #ffffff !important;
    }

    .bg-blue-dark tr td {
      color: #ffffff !important;
    }

    .bg-white {
      background-color: #ffffff;
    }

    .mail-content {
      width: 600px;
      margin: auto;
    }

    .social img {
      width: 40px;
      height: 40px;
    }

    footer {
      display: flex;
      align-items: center;
      justify-content: space-between;
    }

    @media (max-width: 640px) {
      .mail-content {
        width: 90%;
      }

      footer {
        flex-direction: column;
      }

      .social {
        margin-top: 1rem;
      }
    }
  </style>
</head>
<body>
<div class="mail-content">
  <header style="text-align: center" class="p-1 bg-blue-dark">
    <a href="https://yinfoc.com/" target="_blank">
      <img src="https://cdn.yinfoc.com/logo.png" width="165px" alt="Yinfoc - Dẫn lối thành công">
    </a>
  </header>
  <section class="p-1 bg-white py-2">
    @yield('content')
  </section>
  <footer class="p-1 bg-blue-dark">
    <div class="f-contact">
      Phone: <a class="text-white" href="tel:{{$companyInfo->phone}}">{{$companyInfo->phone}}</a><br>
      Mail: <a class="text-white" href="mailto:{{$companyInfo->mail_support}}">{{$companyInfo->mail_support}}</a>
    </div>
    <div class="social">
      <a href="{{$companyInfo->facebook}}">
        <img src="http://cdn.yinfoc.com/icon/facebook.png" alt="Yinfoc - Dẫn lối thành công">
      </a>
      <a href="{{$companyInfo->dribble}}">
        <img src="http://cdn.yinfoc.com/icon/dribble.png" alt="Yinfoc - Dẫn lối thành công">
      </a>
      <a href="{{$companyInfo->behance}}">
        <img src="http://cdn.yinfoc.com/icon/behance.png" alt="Yinfoc - Dẫn lối thành công">
      </a>
      <a href="{{$companyInfo->instagram}}">
        <img src="http://cdn.yinfoc.com/icon/instagram.png" alt="Yinfoc - Dẫn lối thành công">
      </a>
    </div>
  </footer>
</div>
</body>
</html>
