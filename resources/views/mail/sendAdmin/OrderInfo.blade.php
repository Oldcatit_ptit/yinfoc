@extends('mail.mailTemplate')

@section('content')
  <h2>Thông tin đơn hàng</h2>
  <hr>
  <table>
    <tr>
      <td>Mã đơn hàng</td>
      <td>: <b class="text-orange">{{$order->code}}</b></td>
    </tr>
    <tr>
      <td>Khách hàng</td>
      <td>: {{$order->name}}</td>
    </tr>
    <tr>
      <td>Dịch vụ/ Sản phẩm</td>
      <td>: {{$product->name}} - ID:{{$product->id}}</td>
    </tr>
    <tr>
      <td>Gói dịch vụ</td>
      <td>: {{$order->package}}</td>
    </tr>
    <tr>
      <td>Số điện thoại</td>
      <td>: {{$order->phone}}</td>
    </tr>
    <tr>
      <td>Email</td>
      <td>: {{$order->mail}}</td>
    </tr>
    <tr>
      <td>Yêu cầu</td>
      <td>: {{$order->des}}</td>
    </tr>
  </table>
  <hr>
  <div class="text-center py-2">
    <a href="@if($order->type == 1) {{url('yinadmin/order/product')}} @else {{url('yinadmin/order/service')}} @endif" class="btn-orange">
      Quản lý đơn hàng
    </a>
  </div>
@endsection

