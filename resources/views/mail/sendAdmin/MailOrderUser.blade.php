@extends('mail.mailTemplate')

@section('content')
  <div>
    <p>Kính chào quý khách <strong>{{$order->name}}</strong>,</p>
    <p>Quý khách vừa đặt hàng gói giao diện với Yinfoc tại website
      <a href="https://www.yinfoc.com" target="_blank">www.yinfoc.com</a>
      . Dưới đây là các thông tin mà quý khách đã gửi cho Yinfoc.
    </p>
  </div>
  <hr>
  <h2>Thông tin đơn hàng</h2>
  <table>
    <tr>
      <td>Mã đơn hàng</td>
      <td>: <b class="text-orange">{{$order->code}}</b></td>
    </tr>
    <tr>
      <td>Khách hàng</td>
      <td>: {{$order->name}}</td>
    </tr>
    <tr>
      <td>Dịch vụ/ Sản phẩm</td>
      <td>: {{$product->name}} - ID:{{$product->id}}</td>
    </tr>
    <tr>
      <td>Gói dịch vụ</td>
      <td>:
        @if ($order->package == 1)
            3 tháng - {{ number_format(env('PRICE_PACKAGE_'.$order->package.'')) }} VNĐ
        @elseif ($order->package == 2)
            6 tháng - {{ number_format(env('PRICE_PACKAGE_'.$order->package.'')) }} VNĐ
        @elseif ($order->package == 3)
            12 tháng - {{ number_format(env('PRICE_PACKAGE_'.$order->package.'')) }} VNĐ
        @endif
      </td>
    </tr>
    <tr>
      <td>Số điện thoại</td>
      <td>: {{$order->phone}}</td>
    </tr>
    <tr>
      <td>Email</td>
      <td>: {{$order->mail}}</td>
    </tr>
    {{-- <tr>
      <td>Yêu cầu</td>
      <td>: {{$order->des}} </td>
    </tr> --}}
  </table>
  <div>
    <p  style="margin: 0;padding-left: 3px;border-collapse: separate;
                white-space: normal;
                line-height: normal;
                font-weight: normal;
                font-size: medium;
                font-style: normal;
                color: -internal-quirk-inherit;
                text-align: start;
                border-spacing: 2px;
                font-variant: normal;
                ">
        Yêu cầu thêm:
    </p>
    <p style="margin: 0;padding-left: 3px;">
        {{$order->des}}</p>
  </div>
  <hr>
  <h2>Hướng dẫn thanh toán</h2>
  <p>Quý khách vui lòng thanh toán bằng hình thức chuyển khoản ngân hàng với nội dung:</p>
  <p>
    [<span class="text-orange">Mã đơn hàng</span>]_
    [<span class="text-orange">Số điện thoại</span>]_
    [<span class="text-orange">Họ và tên</span>]
  </p>
  <table>
    <tr>
      <td>
        <b>Ngân hàng: {{env('BANK_NAME1')}}</b><br>
        Chủ TK: {{env('BANK_USER1')}} <br>
        Chi nhánh: {{env('BANK_ADDRESS1')}} <br>
        STK: {{env('BANK_NUMBER1')}}
      </td>
    </tr>
    <tr>
      <td>
        <b>Ngân hàng: {{env('BANK_NAME2')}}</b> <br>
        Chủ TK: {{env('BANK_USER2')}} <br>
        Chi nhánh: {{env('BANK_ADDRESS2')}} <br>
        STK: {{env('BANK_NUMBER2')}}
      </td>
    </tr>
  </table>
  <hr>
  <div>
    <p>
      <b><i>Cảm ơn Quý khách đã đặt hàng gói sản phẩm!</i></b>
    </p>
  </div>
@endsection

