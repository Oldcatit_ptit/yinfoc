@extends('admin.template')
@section('content')
  <section class="content-header">
    <h1>
      Sửa danh mục
      <small>{{$data->name}}</small>
    </h1>
  </section>
  <section class="content">
    <div class="box">
      <form action="{{url('yinadmin/feedback/'.$data->id)}}" method="post" enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <div class="box-header with-border">
          <h4 class="modal-title">Thông tin danh mục</h4>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-4">
              <div class="y-img y-img--1x1 mb-3" id="ip_thumb_reader" style="background-image: url({{asset('storage/'.$data->image)}})">
                <div class="y-img__input">
                  <i class="fa fa-picture-o fa-2x"></i>
                  <input name="image" type="file" id="ip_thumb">
                </div>
              </div>
            </div>
            <div class="col-md-8">
              <div class="form-group">
                <label>Tên khách hàng</label>
                <input value="{{$data->name}}" name="name" type="text" class="form-control" placeholder="Tên sản phẩm">
              </div>
              <div class="form-group">
                <label>Mô tả</label>
                <input value="{{$data->sub}}" name="name_sub" type="text" class="form-control" placeholder="Tên sản phẩm">
              </div>
              <div class="form-group">
                <label>Đánh giá</label>
                <select name="rank" class="form-control">
                  @for( $i = 1; $i<=5; $i++ )
                    <option value="{{$i}}" @if($i == $data->rank) selected @endif>{{$i}}</option>
                  @endfor
                </select>
              </div>
              <div class="form-group">
                <label>Nội dung</label>
                <textarea rows="5" name="content" type="text" class="form-control text-left" placeholder="nội dung">{{$data->content}}</textarea>
              </div>
              <div class="form-group">
                <label>Trạng thái</label>
                <select name="rank" class="form-control">
                  @for( $i = 0; $i<=1; $i++ )
                    <option value="{{$i}}" @if($i == $data->status) selected @endif>
                      @if($i == 1) Hiển thị @else Ẩn @endif
                    </option>
                  @endfor
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="box-footer text-right">
          <a href="{{url('yinadmin/options')}}" class="btn btn-default" data-dismiss="modal">Hủy</a>
          <button data-toggle="modal" data-target="#modal-update" type="button" class="btn btn-primary">Cập nhật</button>
        </div>
        {{-- Alert update --}}
        <div class="modal fade" id="modal-update" style="display: none;">
          <div class="modal-dialog modal-sm">
            <div class="modal-content text-center">
              <div class="modal-body">
                <h4>Cập nhật thông tin danh mục ?</h4>
                <button type="submit" class="btn btn-sm btn-warning"> Đồng ý </button>
                <button data-dismiss="modal" type="submit" class="btn btn-sm btn-secondary"> Hủy </button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
      </form>
    </div>
  </section>
@endsection

@section('script')
  <script>
    $(document).ready(function () {
      $('#menu-admin-feedback').menuActive()
      $('#ip_thumb').fileChange('#ip_thumb_reader')
    })
  </script>
@endsection
