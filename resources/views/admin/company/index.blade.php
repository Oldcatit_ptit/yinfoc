@extends('admin.template')
@section('content')
  <section class="content-header">
    <h1>
      Thông tin doanh nghiệp
    </h1>
  </section>
  <section class="content">
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Thông tin chung</a></li>
        <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Kênh online</a></li>
        <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Giới thiệu</a></li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
          <form action="{{url('yinadmin/company-info/'.$data->id)}}" method="post">
            @method('PUT')
            @csrf
            <div class="form-group">
              <label>Tên công ty</label>
              <input value="{{$data->name}}" name="name" type="text" class="slug_parent form-control"
                     placeholder="Tên sản phẩm">
            </div>
            <div class="form-group">
              <label>Địa chỉ</label>
              <input value="{{$data->address}}" name="address" type="text" class="slug_parent form-control"
                     placeholder="Tên sản phẩm">
            </div>
            <label>Hotline</label>
            <div class="row">
              <div class="col-md-6">
                <div class="input-group">
                  <span class="input-group-addon">Hotline</span>
                  <input value="{{$data->phone}}" name="phone" type="text" class="slug_parent form-control">
                </div>
              </div>
              <div class="col-md-6">
                <div class="input-group">
                  <span class="input-group-addon">Hỗ trợ kỹ thuật</span>
                  <input value="{{$data->phone_tech}}" name="phone_tech" type="text" class="slug_parent form-control">
                </div>
              </div>
            </div>
            <br>
            <label>Email</label>
            <div class="row">
              <div class="col-md-6">
                <div class="input-group">
                  <span class="input-group-addon">Mail hỗ trợ</span>
                  <input value="{{$data->mail_support}}" name="mail_support" type="text"
                         class="slug_parent form-control">
                </div>
              </div>
              <div class="col-md-6">
                <div class="input-group">
                  <span class="input-group-addon">Mail liên hệ</span>
                  <input value="{{$data->mail_recruitment}}" name="mail_recruitment" type="text"
                         class="slug_parent form-control">
                </div>
              </div>
            </div>
            <br>
            <div class="text-right">
              <button class="btn btn-info" type="submit"> Lưu thông tin</button>
            </div>
            @csrf
          </form>
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="tab_2">
          <form action="{{url('yinadmin/company-info/'.$data->id)}}" method="post">
            @method('PUT')
            @csrf
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-facebook"></i></span>
              <input value="{{$data->facebook}}" name="facebook" type="text" class="slug_parent form-control">
            </div>
            <br>
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-instagram"></i></span>
              <input value="{{$data->instagram}}" name="instagram" type="text" class="slug_parent form-control">
            </div>
            <br>
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-google-plus"></i></span>
              <input value="{{$data->google}}" name="google" type="text" class="slug_parent form-control">
            </div>
            <br>
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-behance"></i></span>
              <input value="{{$data->behance}}" name="behance" type="text" class="slug_parent form-control">
            </div>
            <br>
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-youtube"></i></span>
              <input value="{{$data->youtube}}" name="youtube" type="text" class="slug_parent form-control">
            </div>
            <br>
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-dribbble"></i></span>
              <input value="{{$data->dribbble}}" name="dribbble" type="text" class="slug_parent form-control">
            </div>
            <br>
            <div class="text-right">
              <button class="btn btn-info" type="submit"> Lưu thông tin</button>
            </div>
          </form>
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="tab_3">
          <form action="{{url('yinadmin/company-info/'.$data->id)}}" method="post" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="row">
              <div class="col-md-6">
                <div class="y-img y-img--2x1 mb-3" id="ip_thumb_reader" style="background-image: url({{asset('storage/'.$data->image)}})">
                  <div class="y-img__input">
                    <i class="fa fa-picture-o fa-2x"></i>
                    <input name="image" type="file" id="ip_thumb">
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Tiêu đề</label>
                  <textarea  name="intro_title" type="text" class="form-control">{{$data->intro_title}}</textarea>
                </div>
              </div>
            </div>
            <br>
            <div class="form-group">
              <textarea  name="intro" type="text" class="form-control ckeditor">{{$data->intro}}</textarea>
            </div>
            <br>
            <div class="text-right">
              <button class="btn btn-info" type="submit"> Lưu thông tin</button>
            </div>
          </form>
        </div>
        <!-- /.tab-pane -->
      </div>
      <!-- /.tab-content -->
    </div>
  </section>
@endsection

@section('script')
  <script>
    $(document).ready(function () {
      $('#menu-admin-company').menuActive()
      $('#ip_thumb').fileChange('#ip_thumb_reader')
    })
  </script>
@endsection

