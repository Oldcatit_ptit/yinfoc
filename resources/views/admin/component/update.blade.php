@extends('admin.template')
@section('content')
  <section class="content-header">
    <h1>
      @if($data->type == 'banner')
        Sửa banner
      @elseif($data->type == 'product')
        Sửa sản phẩm nổi bật
      @endif
      <small>{{$data->name}}</small>
    </h1>
  </section>
  <section class="content">
    <div class="box">
      <form action="{{url('yinadmin/components/'.$data->id)}}" method="post" enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <div class="box-header with-border">
          <h4 class="modal-title">Thông tin</h4>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-4">
              <div class="y-img y-img--4x3 mb-3" id="ip_thumb_reader"
                   style="background-image: url({{asset('storage/'.$data->image)}})">
                <div class="y-img__input">
                  <i class="fa fa-picture-o fa-2x"></i>
                  <input name="image" type="file" id="ip_thumb">
                </div>
              </div>
            </div>
            <div class="col-md-8">
              <div class="form-group">
                <label>Tên</label>
                <input value="{{$data->name}}" name="name" type="text" class="slug_parent form-control">
              </div>
              <div class="form-group">
                <label>Mô tả</label>
                <input value="{{$data->des}}" name="des" type="text" class="slug_parent form-control">
              </div>
              <div class="form-group">
                <label>Link liên kết</label>
                <input value="{{$data->link}}" name="link" type="text" class="slug_parent form-control">
              </div>
              <div class="form-group">
                <label>Tên liên kết</label>
                <input value="{{$data->link_name}}" name="link_name" type="text" class="slug_parent form-control">
              </div>
            </div>
          </div>
        </div>
        <div class="box-footer text-right">
          @if($data->type == "product")
          <a href="{{url('yinadmin/component/products')}}" class="btn btn-default" data-dismiss="modal">Hủy</a>
          @elseif($data->type == "banner")
            <a href="{{url('yinadmin/component/banners')}}" class="btn btn-default" data-dismiss="modal">Hủy</a>
          @endif
          <button data-toggle="modal" data-target="#modal-update" type="button" class="btn btn-primary">Cập nhật</button>
        </div>
        {{-- Alert update --}}
        <div class="modal fade" id="modal-update" style="display: none;">
          <div class="modal-dialog modal-sm">
            <div class="modal-content text-center">
              <div class="modal-body">
                <h4>Cập nhật thông tin danh mục ?</h4>
                <button type="submit" class="btn btn-sm btn-warning"> Đồng ý </button>
                <button data-dismiss="modal" type="submit" class="btn btn-sm btn-secondary"> Hủy </button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
      </form>
    </div>
  </section>
@endsection

@section('script')
  <script>
    $(document).ready(function () {
      $('#menu-admin-component').menuActive()
      $('#ip_thumb').fileChange('#ip_thumb_reader')
    })
  </script>
@endsection
