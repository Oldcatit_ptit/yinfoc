@extends('admin.template')
@section('content')
  <section class="content-header">
    <h1>
      Tiêu đề page
      <small>Mô tả</small>
    </h1>
  </section>
  <section class="content">
    <div class="box">
      <div class="box-header">
        <div class="pull-left">
          <button class="btn btn-success" data-toggle="modal" data-target="#modal-create"><i class="fa fa-plus"></i>
            Thêm mới
          </button>
        </div>
        <form class="pull-right form-inline" action="#">
          <input type="text" name="table_search" class="form-control" placeholder="Search">
          <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
        </form>
      </div>
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover table-striped">
          <tbody>
          <tr>
            <th width="100px">Tool</th>
          </tr>
          <tr>
            @if(isset($data) && sizeof($data) )
              @foreach( $data as $item )
                <td>
                  <a href="product-categories/{{$item->id}}/edit" class="btn btn-sm btn-primary"> <i
                      class="fa fa-pencil-square-o"></i></a>

                  <button data-toggle="modal" data-target="#modal-del-{{$item->id}}" class="btn btn-sm btn-secondary">
                    <i class="fa fa-trash-o"></i>
                  </button>
                </td>
                {{-- Alert delete --}}
                <div class="modal fade" id="modal-del-{{$item->id}}" style="display: none;">
                  <div class="modal-dialog modal-sm">
                    <div class="modal-content text-center">
                      <div class="modal-body">
                        <h4>Bạn có muốn xóa danh mục ?</h4>
                        <form action="product-categories/{{$item->id}}" method="post" class="d-inline">
                          @method('DELETE')
                          @csrf
                          <button type="submit" class="btn btn-sm btn-warning"> Đồng ý</button>
                          <button data-dismiss="modal" type="submit" class="btn btn-sm btn-secondary"> Hủy</button>
                        </form>
                      </div>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>
              @endforeach
            @endif
          </tr>
          </tbody>
        </table>
      </div>
    </div>
  </section>

  {{-- Form thêm mới--}}
  <div class="modal fade" id="modal-create" style="display: none;">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <form action="{{url('yinadmin/product-categories')}}" method="post" enctype="multipart/form-data">
          @csrf
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Thêm sản phẩm mới</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-4">
                <div class="y-img y-img--2x1 mb-3" id="ip_thumb_reader">
                  <div class="y-img__input">
                    <i class="fa fa-picture-o fa-2x"></i>
                    <input name="image" type="file" required id="ip_thumb">
                  </div>
                </div>
              </div>
              <div class="col-md-8">
                <div class="form-group">
                  <label>Tên danh mục</label>
                  <input name="name" type="text" class="slug_parent form-control" placeholder="Tên sản phẩm">
                </div>
                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-link"></span></span>
                    <input name="slug" type="text" class="slug_child form-control" placeholder="Đường dẫn tĩnh">
                  </div>
                </div>
                <div class="form-group">
                  <textarea name="des" type="text" class="form-control" placeholder="Mô tả ngắn"></textarea>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
            <button type="submit" class="btn btn-primary">Thêm danh mục</button>
          </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
@endsection

@section('script')
  <script>
    $(document).ready(function () {
      $('#menu-admin-product, #menu-admin-product-category').menuActive()
      $('.select2').select2()
      $('#ip_thumb').fileChange('#ip_thumb_reader')
    })
  </script>
@endsection
