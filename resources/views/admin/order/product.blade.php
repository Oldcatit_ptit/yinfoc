@extends('admin.template')
@section('content')
  <section class="content-header">
    <h1>
      Danh sách đơn hàng
      <small>Đặt hàng kho giao diện</small>
    </h1>
  </section>
  <section class="content">
    <div class="box">
      <div class="box-header">
        <form class="pull-right form-inline" action="#">
          <input type="text" name="table_search" class="form-control" placeholder="Search">
          <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
        </form>
      </div>
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover table-striped">
          <tbody>
          <tr>
            <th width="80">MGD</th>
            <th>Giao diện</th>
            <th>Gía theo gói</th>
            <th>Khách hàng</th>
            <th>Số điện thoại</th>
            <th>Email</th>
            <th>Địa chỉ</th>
            <th width="300px">Mô tả</th>
            <th>Ngày</th>
            <th width="100px">Tool</th>
          </tr>
          @if(isset($data) && sizeof($data) )
            @foreach( $data as $item )
              <tr>
                <td>{{$item->code}}</td>
                <td>
                  <?php
                  if (isset($item->products))
                    foreach ($item->products as $products) {
                      echo "<a href='" . url('landingpage/' . $products->slug) . "' target='_blank'>" . $products->slug . "</a>";
                    }
                  ?>
                </td>
                <td>
                  @if($item->package === 1)
                    {{number_format(env('PRICE_PACKAGE_1'),0,'.', '.')}} vnđ
                  @elseif($item->package === 2)
                    {{number_format(env('PRICE_PACKAGE_2'),0,'.', '.')}} vnđ
                  @elseif($item->package === 3)
                    {{number_format(env('PRICE_PACKAGE_3'),0,'.', '.')}} vnđ
                  @endif
                </td>
                <td>{{$item->name}}</td>
                <td>{{$item->phone}}</td>
                <td>{{$item->mail}}</td>
                <td>{{$item->address}}</td>
                <td>{{$item->des}}</td>
                <td>{{$item->created_at}}</td>
                <td>
                  {{--<a href="product-categories/{{$item->id}}/edit" class="btn btn-sm btn-primary"> <i--}}
                  {{--class="fa fa-eye"></i></a>--}}
                </td>
              </tr>
            @endforeach
          @endif
          </tbody>
        </table>
      </div>
    </div>
  </section>
@endsection

@section('script')
  <script>
    $(document).ready(function () {
      $('#menu-admin-order, #menu-admin-order-product').menuActive()
      $('.select2').select2()
      $('#ip_thumb').fileChange('#ip_thumb_reader')
    })
  </script>
@endsection
