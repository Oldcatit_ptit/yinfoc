<aside class="main-sidebar scroll-custom">
  <ul class="sidebar-menu tree" data-widget="tree">
    <li class="header">MAIN NAVIGATION</li>
    <li id="menu-admin-home">
      <a href="#">
        <i class="fa fa-dashboard"></i> <span>Thống kê</span>
      </a>
    </li>

    <li class="treeview" id="menu-admin-component">
      <a href="#">
        <i class="fa fa-desktop text-red"></i> <span>Giao diện</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-right pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li id="menu-admin-component-product"><a href="{{route('component-product')}}">Sản phẩm nổi bật</a></li>
        <li id="menu-admin-component-banner"><a href="{{route('component-banner')}}">Banner trang chủ</a></li>
      </ul>
    </li>
    <li class="treeview">
      <a href="#">
        <i class="fa fa-file-text text-blue"></i> <span>Tin tức</span>
        <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="index.html">Danh sách tin</a></li>
        <li><a href="index2.html">Chuyên mục</a></li>
      </ul>
    </li>

    <li class="treeview">
      <a href="#">
        <i class="fa fa-gift text-orange"></i> <span>Khuyến mãi</span>
        <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="index.html">Voucher</a></li>
        <li><a href="index2.html">Áp dụng trực tiếp</a></li>
      </ul>
    </li>

    <li class="treeview" id="menu-admin-product">
      <a href="{{url('yinadmin/products')}}">
        <i class="fa fa-shopping-cart text-orange"></i> <span>Sản phẩm</span>
        <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                </span>
      </a>
      <ul class="treeview-menu">
        <li id="menu-admin-product_list"><a href="{{url('yinadmin/products')}}">Danh sách sản phẩm</a></li>
        <li id="menu-admin-product-category"><a href="{{url('yinadmin/product-categories')}}">Danh mục</a></li>
        <li id="menu-admin-product-option"><a href="{{url('yinadmin/options')}}">Tùy chọn chức năng</a></li>
      </ul>
    </li>

    <li id="menu-admin-service">
      <a href="{{url('yinadmin/services')}}">
        <i class="fa fa-coffee text-blue-sky"></i> <span>Dịch vụ</span>
      </a>
    </li>

    <li class="treeview" id="menu-admin-order">
      <a href="{{url('yinadmin/order')}}">
        <i class="fa fa-wpforms text-black"></i> <span>Đơn đặt hàng</span>
        <span class="pull-right-container"><i class="fa fa-angle-right pull-right"></i></span>
      </a>
      <ul class="treeview-menu">
        <li id="menu-admin-order-product"><a href="{{url('yinadmin/order/product')}}">Kho giao diện</a></li>
        <li id="menu-admin-order-service"><a href="{{url('yinadmin/order/service')}}">Dịch vụ</a></li>
      </ul>
    </li>

    <li id="menu-admin-feedback">
      <a href="{{url('yinadmin/feedback')}}">
        <i class="fa fa-rss text-red"></i> <span>Phản hồi khách hàng</span>
      </a>
    </li>

    <li id="menu-admin-code">
      <a href="{{url('yinadmin/integratedCode')}}">
        <i class="fa fa-code text-blue-sky"></i> <span>Mã tích hợp</span>
      </a>
    </li>

    <li id="menu-admin-company">
      <a href="{{url('yinadmin/company-info')}}">
        <i class="fa fa-info-circle text-green"></i> <span>Thông tin công ty</span>
      </a>
    </li>
    <li>
      <a href="#">
        <i class="fa fa-user-circle-o text-black-50"></i> <span>Tài khoản</span>
      </a>
    </li>
  </ul>
</aside>
