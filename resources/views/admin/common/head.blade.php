<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Yinfoc admin</title>

    <link rel="stylesheet" href="{{url('admin/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('admin/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{url('admin/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{url('admin/css/AdminLTE.css')}}">
    <link rel="stylesheet" href="{{url('admin/css/skin-blue.min.css')}}">
    <link rel="stylesheet" href="{{url('admin/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{url('admin/css/common.css')}}">
    <script src="{{url('admin/js/jquery.min.js')}}"></script>
    <script src="{{url('admin/js/bootstrap.min.js')}}"></script>
    <script src="{{url('admin/js/select2.full.min.js')}}"></script>
    <script src="{{url('admin/js/adminlte.min.js')}}"></script>
    <script src="{{url('admin/ckeditor/ckeditor.js')}}"></script>
    <script src="{{url('admin/js/app.js')}}"></script>
</head>
