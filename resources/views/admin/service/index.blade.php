@extends('admin.template')
@section('content')
  <section class="content-header">
    <h1>
      Dịch vụ
      <small>Danh sách dịch vụ</small>
    </h1>
  </section>
  <section class="content">
    <div class="box">
      <div class="box-header">
        <div class="pull-left">
          <button class="btn btn-success" data-toggle="modal" data-target="#modal-create"><i class="fa fa-plus"></i>
            Thêm mới
          </button>
        </div>
        <form class="pull-right form-inline" action="#">
          <input type="text" name="table_search" class="form-control" placeholder="Search">
          <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
        </form>
      </div>
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover table-striped">
          <tbody>
          <tr>
            <th>Ảnh</th>
            <th>Name</th>
            <th>Giá</th>
            <th>Url</th>
            <th width="100px">Tool</th>
          </tr>
          @if(isset($data) && sizeof($data) )
            @foreach( $data as $item )
              <tr>
                <td width="100px">
                  <div class="y-img y-img--3x2"
                       style="background-image: url({{asset('storage/'.$item->image)}})">
                  </div>
                </td>
                <td>{{$item->name}}</td>
                <td>{{$item->price}}</td>
                <td>{{$item->slug}}</td>
                <td>
                  <a href="services/{{$item->id}}/edit" class="btn btn-sm btn-primary"> <i
                      class="fa fa-pencil-square-o"></i></a>

                  <button data-toggle="modal" data-target="#modal-del-{{$item->id}}" class="btn btn-sm btn-secondary">
                    <i class="fa fa-trash-o"></i>
                  </button>
                </td>
                {{-- Alert delete --}}
                <div class="modal fade" id="modal-del-{{$item->id}}" style="display: none;">
                  <div class="modal-dialog modal-sm">
                    <div class="modal-content text-center">
                      <div class="modal-body">
                        <h4>Bạn có muốn xóa danh mục ?</h4>
                        <form action="{{url('yinadmin/services/'.$item->id)}}" method="post" class="d-inline">
                          @method('DELETE')
                          @csrf
                          <button type="submit" class="btn btn-sm btn-warning"> Đồng ý</button>
                          <button data-dismiss="modal" type="submit" class="btn btn-sm btn-secondary"> Hủy</button>
                        </form>
                      </div>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>
              </tr>
            @endforeach
          @endif
          </tbody>
        </table>
      </div>
    </div>
  </section>

  {{-- Form thêm mới--}}
  <div class="modal fade" id="modal-create" style="display: none;">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <form action="{{url('yinadmin/services')}}" method="post" enctype="multipart/form-data">
          @csrf
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Thêm dịch vụ</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-4">
                <div class="y-img y-img--2x1 mb-3" id="ip_thumb_reader">
                  <div class="y-img__input">
                    <i class="fa fa-picture-o fa-2x"></i>
                    <input name="image" type="file" required id="ip_thumb">
                  </div>
                </div>
              </div>
              <div class="col-md-8">
                <div class="form-group">
                  <label>Tên dịch vụ</label>
                  <input name="name" type="text" required class="slug_parent form-control" placeholder="Tên dịch vụ">
                </div>
                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-link"></span></span>
                    <input name="slug" required type="text" class="slug_child form-control" placeholder="Đường dẫn tĩnh">
                  </div>
                </div>
                <div class="form-group">
                  <label>Link liên kết</label>
                  <input name="link" type="text" class="slug_parent form-control" placeholder="Link liên kết">
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Giá</label>
                      <input name="price" required type="text" class="slug_parent form-control" placeholder="Giá dịch vụ">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Mô tả sau giá</label>
                      <input name="price_des" required type="text" class="slug_parent form-control" placeholder="Mô tả sau giá">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label>Sup</label>
              <textarea name="sup" type="text" required class="form-control" placeholder="Mô tả"></textarea>
            </div>
            <div class="form-group">
              <label>Chức năng</label><br>
              <small>Nhập cách nhau bởi dấu ' enter '</small>
              <textarea name="option" type="text" required class="form-control" rows="5"></textarea>
            </div>
            <div class="form-group">
              <label>Giới thiệu</label>
              <textarea name="des" type="text" required class="form-control ckeditor" placeholder="Mô tả"></textarea>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
            <button type="submit" class="btn btn-primary">Thêm danh mục</button>
          </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
@endsection

@section('script')
  <script>
    $(document).ready(function () {
      $('#menu-admin-service').menuActive()
      $('#ip_thumb').fileChange('#ip_thumb_reader')
    })
  </script>
@endsection
