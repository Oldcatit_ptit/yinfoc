@extends('admin.template')
@section('content')
  <section class="content-header">
    <h1>
      Sửa dịch vụ
      <small>{{$data->name}}</small>
    </h1>
  </section>
  <section class="content">
    <div class="box">
      <form action="{{url('yinadmin/services/'.$data->id)}}" method="post" enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <div class="box-header with-border">
          <h4 class="modal-title">Thông tin danh mục</h4>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-4">
              <div class="y-img y-img--2x1 mb-3" id="ip_thumb_reader" style="background-image: url({{asset('storage/'.$data->image)}})">
                <div class="y-img__input">
                  <i class="fa fa-picture-o fa-2x"></i>
                  <input name="image" type="file" id="ip_thumb">
                </div>
              </div>
            </div>
            <div class="col-md-8">
              <div class="form-group">
                <label>Tên danh mục</label>
                <input value="{{$data->name}}" name="name" type="text" class="slug_parent form-control" placeholder="Tên sản phẩm">
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><span class="fa fa-link"></span></span>
                  <input value="{{$data->slug}}" name="slug" type="text" class="slug_child form-control" placeholder="Đường dẫn tĩnh">
                </div>
              </div>
              <div class="form-group">
                <label>Link liên kết</label>
                <input value="{{$data->link}}" name="link" type="text" class="slug_parent form-control">
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Giá</label>
                    <input value="{{$data->price}}" name="price" required type="text" class="slug_parent form-control">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Mô tả sau giá</label>
                    <input value="{{$data->price_des}}" name="price_des" required type="text" class="slug_parent form-control">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label>Sup</label><br>
                <small>Nội dung phân cách nhau bởi dấu " . "</small>
                <textarea name="sup" type="text" required class="form-control">{!! $data->sup !!}</textarea>
              </div>
              <div class="form-group">
                <label>Chức năng</label><br>
                <small>Nhập cách nhau bởi dấu ' enter '</small>
                <textarea name="option" type="text" required class="form-control" rows="5">{!! $data->option !!}</textarea>
              </div>
              <div class="form-group">
                <textarea name="des" type="text" class="form-control ckeditor" placeholder="Mô tả ngắn">{{$data->des}}</textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="box-footer text-right">
          <a href="{{url('yinadmin/services')}}" class="btn btn-default" data-dismiss="modal">Hủy</a>
          <button data-toggle="modal" data-target="#modal-update" type="button" class="btn btn-primary">Cập nhật</button>
        </div>
        {{-- Alert update --}}
        <div class="modal fade" id="modal-update" style="display: none;">
          <div class="modal-dialog modal-sm">
            <div class="modal-content text-center">
              <div class="modal-body">
                <h4>Cập nhật thông tin danh mục ?</h4>
                <button type="submit" class="btn btn-sm btn-warning"> Đồng ý </button>
                <button data-dismiss="modal" type="submit" class="btn btn-sm btn-secondary"> Hủy </button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
      </form>
    </div>
  </section>
@endsection

@section('script')
  <script>
    $(document).ready(function () {
      $('#menu-admin-service').menuActive()
      $('.select2').select2()
      $('#ip_thumb').fileChange('#ip_thumb_reader')
    })
  </script>
@endsection
