<!DOCTYPE html>
<html lang="en">
@include('admin.common.head')
<body class="skin-blue sidebar-mini fixed">
@include('admin.common.header')
@include('admin.common.menu')
<div class="content-wrapper">
    @yield('content')
</div>

@include('admin.common.footer')

@yield('script')
</body>
</html>




