@extends('admin.template')
@section('content')
  <section class="content-header">
    <h1>
      Tùy chọn
      <small>Thêm tùy chọn cho sản phẩm</small>
    </h1>
  </section>
  <section class="content">
    <div class="box">
      <div class="box-header">
        <div class="pull-left">
          <button class="btn btn-success" data-toggle="modal" data-target="#modal-create"><i class="fa fa-plus"></i>
            Thêm mới
          </button>
        </div>
        <form class="pull-right form-inline" action="#">
          <input type="text" name="table_search" class="form-control" placeholder="Search">
          <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
        </form>
      </div>
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover table-striped">
          <tbody>
          <tr>
            <th>Tên</th>
            <th>Mô tả</th>
            <th width="100px">Tool</th>
          </tr>
          @if(isset($data) && sizeof($data) )
            @foreach( $data as $item )
              <tr>
                <td>{{$item->name}}</td>
                <td>{{$item->des}}</td>
                <td>
                  <a href="options/{{$item->id}}/edit" class="btn btn-sm btn-primary"> <i
                      class="fa fa-pencil-square-o"></i></a>

                  <button data-toggle="modal" data-target="#modal-del-{{$item->id}}" class="btn btn-sm btn-secondary">
                    <i class="fa fa-trash-o"></i>
                  </button>
                </td>
              </tr>
              {{-- Alert delete --}}
              <div class="modal fade" id="modal-del-{{$item->id}}" style="display: none;">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content text-center">
                    <div class="modal-body">
                      <h4>Bạn có muốn xóa option "{{$item->name}}" ?</h4>
                      <form action="options/{{$item->id}}" method="post" class="d-inline">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-sm btn-warning"> Đồng ý</button>
                        <button data-dismiss="modal" type="submit" class="btn btn-sm btn-secondary"> Hủy</button>
                      </form>
                    </div>
                  </div>
                  <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
              </div>
            @endforeach
          @endif
          </tbody>
        </table>
      </div>
    </div>
  </section>

  {{-- Form thêm mới--}}
  <div class="modal fade" id="modal-create" style="display: none;">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <form action="{{url('yinadmin/options')}}" method="post" enctype="multipart/form-data">
          @csrf
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Thêm tùy chọn</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <input name="name" type="text" class="form-control" placeholder="Tên">
                </div>
                <div class="form-group">
                  <textarea name="des" type="text" class="form-control" placeholder="Mô tả ngắn"></textarea>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
            <button type="submit" class="btn btn-primary">Thêm tùy chọn</button>
          </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
@endsection

@section('script')
  <script>
    $(document).ready(function () {
      $('#menu-admin-product, #menu-admin-product-option').menuActive()
    })
  </script>
@endsection
