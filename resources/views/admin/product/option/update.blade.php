@extends('admin.template')
@section('content')
  <section class="content-header">
    <h1>
      Sửa tùy chọn sản phẩm
      <small>{{$data->name}}</small>
    </h1>
  </section>
  <section class="content">
    <div class="box">
      <form action="{{url('yinadmin/options/'.$data->id)}}" method="post" enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <div class="box-header with-border">
          <h4 class="modal-title">Thông tin tùy chọn sản phẩm</h4>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Tên danh mục</label>
                <input value="{{$data->name}}" name="name" type="text" class="form-control" placeholder="Tên sản phẩm">
              </div>
              <div class="form-group">
                <textarea name="des" type="text" class="form-control text-left" placeholder="Mô tả ngắn">
                  {{$data->des}}
                </textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="box-footer text-right">
          <a href="{{url('yinadmin/options')}}" type="button" class="btn btn-default">Hủy</a>
          <button data-toggle="modal" data-target="#modal-update" type="button" class="btn btn-primary">Cập nhật danh mục</button>
        </div>
        {{-- Alert update --}}
        <div class="modal fade" id="modal-update" style="display: none;">
          <div class="modal-dialog modal-sm">
            <div class="modal-content text-center">
              <div class="modal-body">
                <h4>Cập nhật thông tin tùy chọn ?</h4>
                <button type="submit" class="btn btn-sm btn-warning"> Đồng ý </button>
                <button data-dismiss="modal" type="submit" class="btn btn-sm btn-secondary"> Hủy </button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
      </form>
    </div>
  </section>
@endsection

@section('script')
  <script>
    $(document).ready(function () {
      $('#menu-admin-product, #menu-admin-product-category').menuActive()
      $('.select2').select2()
      $('#ip_thumb').fileChange('#ip_thumb_reader')
    })
  </script>
@endsection
