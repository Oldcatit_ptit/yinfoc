@extends('admin.template')
@section('content')
  <section class="content-header">
    <h1>
      Cập nhật thông tin sản phẩm
      <small>{{$data['product']->name}}</small>
    </h1>
  </section>
  <section class="content">
    <div class="box">
      <form action="{{url('yinadmin/products/'.$data['product']->id)}}" method="post"
            enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <div class="box-header with-border">
          <h4 class="modal-title">Thông tin sản phẩm</h4>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-8 ">
              <div class="row">
                <div class="col-xs-6">
                  <div id="ip_thumb_reader1" class="y-img y-img--3x2 mb-3"
                       style="background-image: url({{asset('storage/'.explode(',', $data['product']->image)[0])}})">
                    <div class="y-img__input">
                      <i class="fa fa-picture-o fa-2x"></i>
                      <input id="ip_thumb1" name="image1" type="file">
                    </div>
                  </div>
                </div>
                <div class="col-xs-6">
                  <div class="y-img y-img--3x2" id="ip_thumb_reader2"
                       style="background-image: url({{asset('storage/'.explode(',', $data['product']->image)[1])}})">
                    <div class="y-img__input">
                      <i class="fa fa-picture-o fa-2x"></i>
                      <input name="image2" id="ip_thumb2" type="file">
                    </div>
                  </div>
                </div>
              </div>
              <br>
              <div class="form-group">
                            <textarea name="des" type="text"
                                      class="form-control ckeditor">{{$data['product']->des}}</textarea>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Tên sản phẩm</label>
                <input name="name" type="text" class="form-control slug_parent"
                       placeholder="Tên sản phẩm" value="{{$data['product']->name}}">
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><span class="fa fa-link"></span></span>
                  <input name="slug" type="text" class="slug_child form-control"
                         placeholder="Đường dẫn tĩnh" value="{{$data['product']->slug}}">
                </div>
              </div>
              <div class="form-group">
                <label>Danh mục sản phẩm</label>
                <input class="value-select2" name="productCategory" type="text" hidden>
                <select class="form-control select2" multiple="multiple"
                        data-placeholder="Select a State"
                        style="width: 100%;">
                  @foreach($data['categories'] as $category)
                    <option @if(in_array($category->id,$data['productCate'])) selected
                            @endif value="{{$category->id}}">{{$category->name}}</option>
                  @endforeach
                </select>
              </div>
              <div class="row">
                <div class="form-group col-md-6">
                  <label>Giá</label>
                  <div class="input-group">
                    <input value="{{$data['product']->price}}" name="price" type="number"
                           class="form-control">
                    <span class="input-group-addon">vnđ</span>
                  </div>
                </div>
                <div class="form-group col-md-6">
                  <label>Giá sale</label>
                  <div class="input-group">
                    <input value="{{$data['product']->price_sale}}" name="price_sale" type="number"
                           class="form-control">
                    <span class="input-group-addon">vnđ</span>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label>Link demo</label>
                <div class="input-group">
                  <span class="input-group-addon">www</span>
                  <input value="{{$data['product']->url_demo}}" name="url_demo" type="text"
                         class="form-control">
                </div>
              </div>

              <div class="form-group">
                <label>Chức năng</label>
                <?php
                $functions = explode('|', $data['product']->functions);
                ?>
                @foreach( $data['options'] as $item)
                  <div class="checkbox">
                    <label>
                      <input value="{{$item->name}}" @if(in_array($item->name,$functions)) checked
                             @endif type="checkbox" name="functions[]">
                      {{$item->name}}
                    </label>
                  </div>
                @endforeach
              </div>
            </div>
          </div>
        </div>
        <div class="box-footer text-right">
          <a href="{{url('yinadmin/products')}}" class="btn btn-default">Hủy</a>
          <button data-toggle="modal" data-target="#modal-update" type="button" class="btn btn-primary">Cập
            nhật sản phẩm
          </button>
        </div>
        {{-- Alert update --}}
        <div class="modal fade" id="modal-update" style="display: none;">
          <div class="modal-dialog modal-sm">
            <div class="modal-content text-center">
              <div class="modal-body">
                <h4>Cập nhật thông tin sản phẩm ?</h4>
                <button type="submit" class="btn btn-sm btn-warning"> Đồng ý</button>
                <button data-dismiss="modal" type="submit" class="btn btn-sm btn-secondary"> Hủy
                </button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
      </form>
    </div>
  </section>
@endsection

@section('script')
  <script>
    $(document).ready(function () {
      $('#menu-admin-product, #menu-admin-product_list').menuActive()
      $('#ip_thumb1').fileChange('#ip_thumb_reader1')
      $('#ip_thumb2').fileChange('#ip_thumb_reader2')
    })
  </script>
@endsection
