@extends('admin.template')
@section('content')
  <section class="content-header">
    <h1>
      Sản phẩm
      <small>Danh sách</small>
    </h1>
  </section>
  <section class="content">
    <div class="box">
      <div class="box-header">
        <div class="pull-left">
          <button class="btn btn-success" data-toggle="modal" data-target="#modal-default"><i
              class="fa fa-plus"></i> Thêm mới
          </button>
        </div>

        <form class="pull-right form-inline" action="#">
          <select class="form-control">
            <option>-- Tất cả danh mục --</option>
            @foreach($data['categories'] as $category)
              <option value="{{$category->id}}">{{$category->name}}</option>
            @endforeach
          </select>
          <input type="text" name="table_search" class="form-control" placeholder="Search">
          <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
        </form>
      </div>
      <!-- /.box-header -->
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover table-striped">
          <tbody>
          <tr>
            <th>ID</th>
            <th>Ảnh</th>
            <th>Name</th>
            <th>Price</th>
            <th>Status</th>
            <th>Link demo</th>
            <th>Date</th>
            <th>Tool</th>
          </tr>
          @if(isset($data['products']) && sizeof($data['products']) )
            @foreach( $data['products'] as $item )
              <tr>
                <td width="40px" class="text-center">{{$item->id}}</td>
                <td width="100px">
                  <div class="y-img y-img--3x2"
                       style="background-image: url({{asset('storage/'.explode(',', $item->image)[0])}})"></div>
                </td>
                <td>{{$item->name}}</td>
                <td>{{$item->price}}</td>
                <td>
                  <span class="label label-success">Approved</span>
                </td>
                <td><a href="{{$item->url_demo}}" target="_blank">{{$item->url_demo}}</a></td>
                <td>{{date_format(date_create($item->created_at),"d/m/Y")}}</td>
                <td width="100px">
                  <a href="{{url('yinadmin/products/'.$item->id.'/edit')}}" class="btn btn-sm btn-primary">
                    <i class="fa fa-pencil-square-o"></i>
                  </a>
                  <button data-toggle="modal" data-target="#modal-del-{{$item->id}}"
                          class="btn btn-sm btn-secondary">
                    <i class="fa fa-trash-o"></i>
                  </button>
                </td>
              </tr>
              {{-- Alert delete --}}
              <div class="modal fade" id="modal-del-{{$item->id}}" style="display: none;">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content text-center">
                    <div class="modal-body">
                      <h4>Bạn có muốn xóa sản phẩm ?</h4>
                      <form action="{{url('yinadmin/products/'.$item->id)}}" method="post"
                            class="d-inline">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-sm btn-warning"> Đồng ý</button>
                        <button data-dismiss="modal" type="submit"
                                class="btn btn-sm btn-secondary"> Hủy
                        </button>
                      </form>
                    </div>
                  </div>
                  <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
              </div>
            @endforeach
          @endif
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
  </section>

  {{-- Form thêm mới--}}
  <div class="modal modal-full fade" id="modal-default" style="display: none;">
    <div class="modal-dialog">
      <div class="modal-content">
        <form action="{{url('yinadmin/products')}}" method="post" enctype="multipart/form-data">
          @csrf
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Thêm sản phẩm mới</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-8 ">
                <div class="row mb-5">
                  <div class="col-xs-6">
                    <div id="ip_thumb_reader1" class="y-img y-img--3x2 mb-3">
                      <div class="y-img__input">
                        <i class="fa fa-picture-o fa-2x"></i>
                        <input id="ip_thumb1" name="image1" type="file" required>
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-6">
                    <div class="y-img y-img--3x2" id="ip_thumb_reader2">
                      <div class="y-img__input">
                        <i class="fa fa-picture-o fa-2x"></i>
                        <input name="image2" id="ip_thumb2" type="file" required>
                      </div>
                    </div>
                  </div>
                </div>
                <br>
                <div class="form-group">
                  <textarea name="des" type="text" class="form-control ckeditor">Nội dung giới thiệu</textarea>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Tên sản phẩm</label>
                  <input name="name" type="text" class="form-control slug_parent"
                         placeholder="Tên sản phẩm">
                </div>
                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-link"></span></span>
                    <input name="slug" type="text" class="slug_child form-control"
                           placeholder="Đường dẫn tĩnh">
                  </div>
                </div>
                <div class="form-group">
                  <label>Danh mục sản phẩm</label>
                  <input class="value-select2" name="productCategory" type="text" hidden>
                  <select class="form-control select2" multiple="multiple"
                          data-placeholder="Select a State"
                          style="width: 100%;">
                    @foreach($data['categories'] as $category)
                      <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="row">
                  <div class="form-group col-md-6">
                    <label>Giá</label>
                    <div class="input-group">
                      <input name="price" type="number" class="form-control">
                      <span class="input-group-addon">vnđ</span>
                    </div>
                  </div>
                  <div class="form-group col-md-6">
                    <label>Giá sale</label>
                    <div class="input-group">
                      <input name="price_sale" type="number" class="form-control">
                      <span class="input-group-addon">vnđ</span>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label>Link demo</label>
                  <div class="input-group">
                    <span class="input-group-addon">www</span>
                    <input name="url_demo" type="text" class="form-control">
                  </div>
                </div>
                <div class="form-group">
                  <label>Chức năng</label>
                  <?php
                  ?>
                  @foreach( $data['options'] as $item)
                    <div class="checkbox">
                      <label>
                        <input value="{{$item->name}}" type="checkbox" name="functions[]">
                        {{$item->name}}
                      </label>
                    </div>
                  @endforeach
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
            <button type="submit" class="btn btn-primary">Thêm sản phẩm</button>
          </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
@endsection

@section('script')
  <script>
    $(document).ready(function () {
      $('#menu-admin-product, #menu-admin-product_list').menuActive()

      $('#ip_thumb1').fileChange('#ip_thumb_reader1')
      $('#ip_thumb2').fileChange('#ip_thumb_reader2')
    })
  </script>
@endsection
