<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Yinfoc thiết kế landing page chuyên nghiệp</title>
    <link rel="stylesheet" href="{{ asset('/admin/css') }}/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('/admin/css') }}/AdminLTE.css">
    <link rel="stylesheet" href="{{ asset('/admin/css') }}/common.css">
    <link rel="stylesheet" href="{{ asset('/admin/css') }}/login.css">
    <link rel="stylesheet" href="{{ asset('/admin/css') }}/font-awesome.min.css">
</head>
  <body class="full-screen">
    <div class="row">
        <div 
            class="col-md-8 full-screen--h login-bg" 
            style="background-image: url({{ asset('/admin/img/login-bg.jpg') }})"
        >
        </div>
        <div class="col-md-4">
            @if (session('error'))
              <div class="alert alert-danger hidden_h" style="position: absolute;right: 100%;width: 100%;top: 10px;">
                <strong>Danger!</strong>{{ session('error') }}
              </div>
            @endif
            <form class="login-form full-screen--h" action="{{ route('admin.post.login') }}" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="login-form__inner text-center">
                    <h4 class="text-center login-title" style="margin-bottom: 10px;">Đăng nhập</h4>
                    <img width="130" src="{{ asset('/admin/img/yinfoc-orange.svg') }}" alt="" style="margin-bottom: 30px;">
                    <div class="form-group has-feedback">
                        <input type="text" name="email" class="form-control" placeholder="Tài khoản" required>
                        <span class="fa fa-user form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" name="password" class="form-control" placeholder="Mật khẩu" required>
                        <span class="fa fa-lock form-control-feedback"></span>
                    </div>
                    <button type="submit" class="btn login-form__btn">Đăng nhập</button>
                </div>
                <div class="row login-support">
                    <div class="col-md-4">
                        <a href="#"><i class="fa fa-book"></i> Hướng dẫn</a>
                    </div>
                    <div class="col-md-4">
                        <a href="tel:0966287678"><i class="fa fa-comments"></i> 0966287678</a>
                    </div>
                    <div class="col-md-4">
                        <a href="logout.php"><i class="fa fa-desktop"></i> Kho giao diện</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $('.hidden_h').delay(5000).slideUp();
        });
    </script>
  </body>
</html>