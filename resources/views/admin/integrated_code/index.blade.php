@extends('admin.template')
@section('content')
  <section class="content-header">
    <h1>
      Tiêu đề page
      <small>Mô tả</small>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-sm-6 col-md-3">
        <form action="{{url('yinadmin/integratedCode/'.$data->id)}}" method="post">
          @method('PUT')
          @csrf
          <div class="box box-info">
            <div class="box-header">
              <h4><b>Mã chatbot</b></h4>
            </div>
            <div class="box-body">
              <textarea name="chatbot" class="form-control" rows="20">{!! $data->chatbot !!}</textarea>
            </div>
            <div class="box-footer">
              <div class="text-right">
                <button class="btn btn-info" type="submit"> Lưu thông tin</button>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="col-sm-6 col-md-3">
        <form action="{{url('yinadmin/integratedCode/'.$data->id)}}" method="post">
          @method('PUT')
          @csrf
          <div class="box box-info">
            <div class="box-header">
              <h4><b>Mã code facebook</b></h4>
            </div>
            <div class="box-body">
              <textarea name="facebook" class="form-control" rows="20">{!! $data->facebook !!}</textarea>
            </div>
            <div class="box-footer">
              <div class="text-right">
                <button class="btn btn-info" type="submit"> Lưu thông tin</button>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="col-sm-6 col-md-3">
        <form action="{{url('yinadmin/integratedCode/'.$data->id)}}" method="post">
          @method('PUT')
          @csrf
          <div class="box box-info">
            <div class="box-header">
              <h4><b>Mã code google</b></h4>
            </div>
            <div class="box-body">
              <textarea name="google" class="form-control" rows="20">{!! $data->google !!}</textarea>
            </div>
            <div class="box-footer">
              <div class="text-right">
                <button class="btn btn-info" type="submit"> Lưu thông tin</button>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="col-sm-6 col-md-3">
        <form action="{{url('yinadmin/integratedCode/'.$data->id)}}" method="post">
          @method('PUT')
          @csrf
          <div class="box box-info">
            <div class="box-header">
              <h4><b>Mã code khác</b></h4>
            </div>
            <div class="box-body">
              <textarea name="other" class="form-control" rows="20">{!! $data->other !!}</textarea>
            </div>
            <div class="box-footer">
              <div class="text-right">
                <button class="btn btn-info" type="submit"> Lưu thông tin</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </section>

@endsection

@section('script')
  <script>
    $(document).ready(function () {
      $('#menu-admin-code').menuActive()
    })
  </script>
@endsection
