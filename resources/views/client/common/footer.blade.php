<div class="footer">
  <div class="footer__t">
    <div class="container">
      <div class="row">
        <div class="mb-5 mb-md-0 col-7 col-sm-12 col-md-5 col-lg-4">
          <img class="footer__logo" src="{{url('img/logo.svg')}}" alt="">
          <p>Yinfoc được tạo lên từ lòng tin của khách hàng. <br>
            Chúng tôi luôn mong muốn tạo ra những giá trị thiết thực trong mỗi sản phẩm.
          </p>
        </div>
        <div class="col-5 col-sm-4 col-md-2">
          <div class="footer__title"><b>Về chúng tôi</b></div>
          <ul>
            <li><a href="#">Trang chủ</a></li>
            <li><a href="{{url('gioi-thieu')}}">Giới thiệu</a></li>
            <li><a href="{{url('lien-he')}}">Liên hệ</a></li>
          </ul>

        </div>
        <div class="col-7 col-sm-5 col-md-3">
          <div class="footer__title"><b>Gói dịch vụ</b></div>
          <ul>
            <li><a href="{{url('kho-giao-dien')}}">Kho giao diện</a></li>
            <li><a href="{{url('dich-vu')}}">Thiết kế Landingpage theo yêu cầu</a></li>
            <li><a href="{{url('dich-vu')}}">Thiết kế logo</a></li>
          </ul>
        </div>
        <div class="col-5 col-sm-3 col-md-2 col-lg-3">
          <div class="footer__title"><b>Liên kết khác</b></div>
          <ul>
            @if(isset($companyInfo->facebook))
              <li><a target="_blank" href="{{$companyInfo->facebook}}">Facebook</a></li>
            @endif
            @if(isset($companyInfo->instagram))
              <li><a target="_blank" href="{{$companyInfo->instagram}}">Instagram</a></li>
            @endif
            @if(isset($companyInfo->behance))
              <li><a target="_blank" href="{{$companyInfo->behance}}">Behance</a></li>
            @endif
            @if(isset($companyInfo->dribbble))
              <li><a target="_blank" href="{{$companyInfo->dribbble}}">Dribbble</a></li>
            @endif
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="footer__bt">
    <div class="container">
      <div class="row">
        <div class="col-sm-7 col-lg-9 mb-3 mb-sm-0">© 2019, YINFOC TEAM</div>
        <div class="col-sm-5 col-lg-3 d-flex justify-content-between">
          <a href="#">Chính sách bảo mật</a>
          <a href="#">Điều khoản sử dụng</a>
        </div>
      </div>
    </div>
  </div>
</div>

 <div id="lioneltawk"></div>

