<section>
  <div class="container">
    <div class="row">
      <div class="col-sm-6 pr-4 mb-4 mb-sm-0" style="margin-top: -13rem">
        <div class="row">
          <div class="col-10 offset-2 mb-4">
            <img src="{{url('img/Rectangle.png')}}" alt="">
          </div>
          <div class="col-8">
            <img src="{{url('img/Rectangle%20Copy%2011.png')}}" alt="">
          </div>
          <div class="col-4">
            <img src="{{url('img/Rectangle%20Copy%2012.png')}}" alt="">
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <p class="text--orange mb-3"><b>HỖ TRỢ KHÁCH HÀNG</b></p>
        <div class="title title--blue">
          <h2>Bạn có thể để lại thông tin để chúng tôi tư vẫn miễn phí cho bạn</h2>
        </div>
        <div class="row">
          <div class="col-md-8">
            <form class="form-basic" action="#" method="post">
              <input type="text" placeholder="Họ và tên">
              <input type="tel" placeholder="Nhập số điện thoại">
              <textarea rows="1" type="text" placeholder="Mô tả yêu cầu"></textarea>
              <button type="submit" class="btn btn--lg btn--orange-gradient w--100 btn--shadow">Gửi yêu cầu <span class="yinicon-arrow-right"></span></button>
            </form>
            <div class="mt-5">
              <p class="text--blue-support"><i>"YINFOC sẽ hỗ trợ các doanh nghiệp tiết kiệm thời gian và tiền bạc để tạo ra các website quảng cáo đúng chuẩn nhất"</i></p>
              <div class="user-tag mt-3">
                <span style="background-image: url({{url('img/vinh-avatar.jpg')}})"></span>
                <div>
                  <p>Lê Quang Vinh</p>
                  <small>Founder Yinfoc</small>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</section>
