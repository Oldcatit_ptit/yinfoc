<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
  <meta property="og:url" content="{{url('/')}}"/>
  <meta property="og:type" content="article"/>
  <meta property="og:title" content="Yinfoc - thiết kế landing page"/>
  <meta property="og:description"
        content="Thiết kế landing page tốc độ load < 2s, tương thích mọi thiết bị, giao diện theo yêu cầu"/>
  <meta property="og:image" content="{{url('image/khuyenmai-mb.png')}}"/>
  <title>Yinfoc</title>
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
        integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <link rel="shortcut icon" href="{{url('favicon.png')}}"/>
{{--  <link rel="stylesheet" href="{{url('css/slick.min.css')}}">--}}
{{--  <link rel="stylesheet" href="{{url('css/aos.css')}}">--}}
{{--  <link rel="stylesheet" href="{{url('css/font.css')}}">--}}
{{--  <link rel="stylesheet" href="{{url('css/mainold.css')}}">--}}
{{--  <link rel="stylesheet" href="{{url('css/main.css')}}">--}}
{{--  <link rel="stylesheet" href="{{url('css/costom.css')}}">--}}
  <link rel="stylesheet" href="{{url('css/combine.css')}}">

{{--  <link--}}
{{--    rel="stylesheet"--}}
{{--    type="text/css"--}}
{{--    href="{{ url('/') }}/combine.php?type=css&files=slick.min.css,aos.css,font.css,mainold.css,main.css,costom.css"--}}
{{--  />--}}
{{--  <script src="{{url('js/jquery2.2.4.min.js')}}"></script>--}}
{{--  <script src="{{url('js/slick.min.js')}}"></script>--}}
{{--  <script src="{{url('js/aos.js')}}"></script>--}}
{{--  <script src="{{url('js/main.js')}}"></script>--}}
{{--  <script src="{{url('js/jquery.lazy.min.js')}}"></script>--}}
<script src="{{url('js/combine.js')}}"></script>

{{--  <script--}}
{{--    src="{{ url('/') }}/combine.php?type=javascript&files=jquery2.2.4.min.js,slick.min.js,aos.js,main.js,jquery.lazy.min.js"--}}
{{--    type="text/javascript"></script>--}}

  <script type="text/javascript">
    $(function () {
      $('.lazyload').lazy();
    });
  </script>

  {!! $code->facebook !!}
  {!! $code->google !!}
  {!! $code->chatbot !!}
  @yield('style')
  <?php setlocale(LC_MONETARY, "en_US"); ?>
</head>
