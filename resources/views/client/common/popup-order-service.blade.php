<div class="modal" id="modal-dk-{{$item->id}}">
  <div class="container">
    <div class="row">
      <div class="col-md-10 offset-md-1">
        <div class="modal-box">
          <div class="modal-inner scroll">
            <div class="row m-0">
              <div class="col-sm-5 p-4">
                @if($item)
                  <div class="prd-card">
                    <div class="prd-card__img bg-img bg-img--43"
                         style="background-image: url({{asset('storage/'.$item->image)}})">
                    </div>
                    <div class="prd-card__txt">
                      <div class="prd-card__info mb-3">
                        <p><b>{{$item->name}}</b></p>
                      </div>
                      <div class="prd-card__price">
                        <p><b>{{$item->price}}</b></p>
                        <p>{{$item->price_des}}</p>
                      </div>
                    </div>
                  </div>
                  <ul class="list">
                    <?php $options = explode(PHP_EOL, $item->option);?>
                    @foreach($options as $o)
                      <li><i class="yinicon-check-orange text--orange"></i> {{$o}}</li>
                    @endforeach
                  </ul>
                @endif
              </div>
              <div class="col-sm-7 bg-blue-gr p-4">
                <form action="{{url('yinadmin/order')}}" class="form-basic" method="post">
                  <div class="title">
                    <h3>Thông tin đăng ký dịch vụ</h3>
                  </div>
                  <?php echo csrf_field(); ?>
                  <div class="box box--shadow-b box-radius px-5 py-4">
                    <input name="products[]" type="text" value="{{$item->id}}" hidden>
                    <input name="type" type="text" value="2" hidden>
                    <input name="slug" type="text" value="{{$item->slug}}" hidden>
                    <input name="name" type="text" placeholder="Họ và tên" required>
                    <input name="phone" type="text" placeholder="Số điện thoại" required>
                    <input name="mail" type="text" placeholder="Email (không bắt buộc)" required>
                    <textarea name="des" rows="1" type="text" placeholder="Lời nhắn của ban"></textarea>
                  </div>

                  <div class="d-flex justify-content-between mt-5 text--blue_light">
                    <div class="d-flex align-items-center pr-4">
                      <i class="yinicon-look mr-4"></i>
                      <i>Thông tin người mua <br>sẽ được đảm bảo giữ bí mật</i>
                    </div>
                    <button class="btn btn--md btn--shadow btn--orange-gradient px-5 py-3">Xác nhận</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div class="modal-close"><b>x</b></div>
        </div>
      </div>
    </div>
  </div>
</div>
