<div class="modal" id="modal-package">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="modal-box" style="background-color: transparent">
          <div class="modal-inner scroll" >
            <div class="row">
              <div class="col-sm-4">
                <div class="package">
                  <div class="package-header p-3">
                    <p class="text--orange">Mẫu giao diện: <br> {{$data->name}} </p>
                    <h4>GÓI 3 THÁNG - {{number_format(env('PRICE_PACKAGE_1'),0,'.', '.')}} vnđ</h4>
                  </div>
                  <div class="package-body px-3 py-2 bg-white">
                    <h5 class="text--orange">Gói cơ bản</h5>
                    <ul>
                      <li>
                        <strike>Tích hợp <b>SSL</b></strike>
                        <span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Những website có bảo mật bằng chứng thư số (SSL) sẽ có những biểu tượng https://, được xác thực bởi tổ chức Let's Encrypt, điều đó giúp cho khách hàng tin tưởng doanh nghiệp của bạn hơn.</p>
                        </span>
                      </li>
                      <li>
                        Tốc độ  <b><3s</b>
                        <span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Cam kết website của bạn sẽ có tốc độ hiển thị dưới 3 giây với điều kiện tốc độ internet tại nơi truy cập bình thường.</p>
                        </span>
                      </li>
                      <li><strike>Miễn phí <b>1 banner</b></strike> <span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Một website đẹp cần phải có ít nhất một banner đẹp để thu hút người dùng. Yinfoc luôn hỗ trợ bạn thực hiện tốt việc này.</p>
                        </span></li>
                      <li><b>10 đơn vị</b> nhập liệu lần đầu<span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Tất cả các gói dịch vụ đều được nhập những dữ liệu quan trọng cơ bản như: thông tin liên hệ, địa chỉ, tên website, logo,… Ngoài ra, tùy theo gói dịch vụ mà bạn được hỗ trợ thêm nhập liệu lần đầu như bài giới thiệu, tầm nhìn, tin tức, sản phẩm,…</p>
                        </span></li>
                    </ul>
                    <ul>
                      <li><strike><b>Miễn phí</b> trang đang xây dựng</strike></li>
                      <li>Hỗ trợ đổi màu chủ đạo miễn phí<span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Yinfoc sẽ thao tác thực tiếp trên máy tính của khách hàng thông qua phần mềm Anydesk. sẽ hỗ trợ bạn đổi màu chủ đạo của website miễn phí trong vòng 7 ngày kể từ ngày đăng ký dịch vụ.</p>
                        </span>
                      </li>
                      <li><strike>Tối ưu hình ảnh</strike></li>
                      <li><strike>Hỗ trợ qua máy tính từ xa</strike><span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Yinfoc sẽ thao tác thực tiếp trên máy tính của khách hàng thông qua phần mềm Anydesk, TeamViewer.</p>
                        </span></li>
                      <li>Tổng đài <b>24/7</b><span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Tổng đài 0358 186 136 luôn nhận cuộc gọi tiếp nhận yêu cầu hỗ trợ của bạn bất kể ngày nghĩ Lễ, Tết.</p>
                        </span></li>
                    </ul>
                    <ul>
                      <li>Nút liên hệ ngay<span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Mở ứng dụng gọi cho bạn tức thời trên điện thoại thông minh mà không cần phải bấm số</p>
                        </span></li>
                      <li>Tắt/Mở Website<span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Bạn toàn quyền tắt hoặc mở landing page. Hữu ích khi bạn muốn chấm dứt ngay 1 chiến dịch</p>
                        </span></li>
                      <li><strike>Kết nối kênh Google Sheet</strike><span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Dữ liệu từ form sẽ được chuyển thẳng 1 chiều vào tài khoản Google Sheets của bạn</p>
                        </span></li>
                      <li>Nhận thông báo về email khi có đơn hàng </li>
                      <li>Nhúng các mã đo lương hiệu quả: Facebook, Google</li>
                      <li>Form thu thập thông tin</li>
                      <li>Quản lý đơn hàng<span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Cho phép bạn xem, chỉnh sửa, xóa, tìm kiếm các đơn hàng mà khách hàng của bạn đã đặt trên website</p>
                        </span></li>
                    </ul>
                  </div>
                  <div class="package-footer p-3 bg-white align-items-center">
                    <div class="text--center">
                      <button id="click-package-1" data-package="1" data-dismiss="#modal-package" data-modal="#modal-dk" class="btn btn--radius btn--orange-gradient btn--md">Đặt mua với gói này</button>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="package">
                  <div class="package-header p-3">
                    <p class="text--orange">Mẫu giao diện: <br>{{$data->name}} </p>
                    <h4>GÓI 6 THÁNG - {{number_format(env('PRICE_PACKAGE_2'),0,'.', '.')}} vnđ</h4>
                  </div>
                  <div class="package-body px-3 py-2 bg-white">
                    <h5 class="text--orange">Giảm <b>20%</b> cho lần mua tiếp theo</h5>
                    <ul>
                      <li>
                        Tích hợp <b>SSL</b>
                        <span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Những website có bảo mật bằng chứng thư số (SSL) sẽ có những biểu tượng https://, được xác thực bởi tổ chức Let's Encrypt, điều đó giúp cho khách hàng tin tưởng doanh nghiệp của bạn hơn.</p>
                        </span>
                      </li>
                      <li>
                        Tốc độ  <b><3s</b>
                        <span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Cam kết website của bạn sẽ có tốc độ hiển thị dưới 3 giây với điều kiện tốc độ internet tại nơi truy cập bình thường.</p>
                        </span>
                      </li>
                      <li>Miễn phí <b>1 banner</b> <span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Một website đẹp cần phải có ít nhất một banner đẹp để thu hút người dùng. Yinfoc luôn hỗ trợ bạn thực hiện tốt việc này.</p>
                        </span></li>
                      <li><b>20 đơn </b> nhập liệu lần đầu <span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Tất cả các gói dịch vụ đều được nhập những dữ liệu quan trọng cơ bản như: thông tin liên hệ, địa chỉ, tên website, logo,… Ngoài ra, tùy theo gói dịch vụ mà bạn được hỗ trợ thêm nhập liệu lần đầu như bài giới thiệu, tầm nhìn, tin tức, sản phẩm,…</p>
                        </span></li>
                    </ul>
                    <ul>
                      <li><strike><b>Miễn phí</b> trang đang xây dựng</strike></li>
                      <li>Hỗ trợ đổi màu chủ đạo miễn phí<span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Yinfoc sẽ thao tác thực tiếp trên máy tính của khách hàng thông qua phần mềm Anydesk. sẽ hỗ trợ bạn đổi màu chủ đạo của website miễn phí trong vòng 7 ngày kể từ ngày đăng ký dịch vụ.</p>
                        </span></li>
                      <li>Tối ưu hình ảnh</li>
                      <li>Hỗ trợ qua máy tính từ xa<span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Yinfoc sẽ thao tác thực tiếp trên máy tính của khách hàng thông qua phần mềm Anydesk, TeamViewer.</p>
                        </span></li>
                      <li>Tổng đài <b>24/7</b><span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Tổng đài 0358 186 136 luôn nhận cuộc gọi tiếp nhận yêu cầu hỗ trợ của bạn bất kể ngày nghĩ Lễ, Tết.</p>
                        </span></li>
                    </ul>
                    <ul>
                      <li>Nút liên hệ ngay<span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Mở ứng dụng gọi cho bạn tức thời trên điện thoại thông minh mà không cần phải bấm số</p>
                        </span></li>
                      <li>Tắt/Mở Website<span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Bạn toàn quyền tắt hoặc mở landing page. Hữu ích khi bạn muốn chấm dứt ngay 1 chiến dịch</p>
                        </span></li>
                      <li>Kết nối kênh Google Sheet<span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Dữ liệu từ form sẽ được chuyển thẳng 1 chiều vào tài khoản Google Sheets của bạn</p>
                        </span></li>
                      <li>Nhận thông báo về email khi có đơn hàng </li>
                      <li>Nhúng các mã đo lương hiệu quả: Facebook, Google</li>
                      <li>Form thu thập thông tin</li>
                      <li>Quản lý đơn hàng<span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Cho phép bạn xem, chỉnh sửa, xóa, tìm kiếm các đơn hàng mà khách hàng của bạn đã đặt trên website</p>
                        </span></li>
                    </ul>
                  </div>
                  <div class="package-footer p-3 bg-white">
                    <div class="text--center">
                      <button id="click-package-2" data-package="2" data-dismiss="#modal-package" data-modal="#modal-dk" class="btn btn--radius btn--orange-gradient btn--md">Đặt mua với gói này</button>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="package">
                  <div class="package-header p-3">
                    <p class="text--orange">Mẫu giao diện: <br>{{$data->name}} </p>
                    <h4>GÓI 1 NĂM - {{number_format(env('PRICE_PACKAGE_3'),0,'.', '.')}} vnđ</h4>
                  </div>
                  <div class="package-body px-3 py-2 bg-white">
                    <h5 class="text--orange">Giảm <b>30%</b> cho lần mua tiếp theo</h5>
                    <ul>
                      <li>
                        Tích hợp <b>SSL</b>
                        <span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Những website có bảo mật bằng chứng thư số (SSL) sẽ có những biểu tượng https://, được xác thực bởi tổ chức Let's Encrypt, điều đó giúp cho khách hàng tin tưởng doanh nghiệp của bạn hơn.</p>
                        </span>
                      </li>
                      <li>
                        Tốc độ  <b><3s</b>
                        <span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Cam kết website của bạn sẽ có tốc độ hiển thị dưới 3 giây với điều kiện tốc độ internet tại nơi truy cập bình thường.</p>
                        </span>
                      </li>
                      <li>Miễn phí <b>1 banner</b> <span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Một website đẹp cần phải có ít nhất một banner đẹp để thu hút người dùng. Yinfoc luôn hỗ trợ bạn thực hiện tốt việc này.</p>
                        </span></li>
                      <li><b>40 đơn vị</b> nhập liệu lần đầu <span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Tất cả các gói dịch vụ đều được nhập những dữ liệu quan trọng cơ bản như: thông tin liên hệ, địa chỉ, tên website, logo,… Ngoài ra, tùy theo gói dịch vụ mà bạn được hỗ trợ thêm nhập liệu lần đầu như bài giới thiệu, tầm nhìn, tin tức, sản phẩm,…</p>
                        </span></li>
                    </ul>
                    <ul>
                      <li><b>Miễn phí</b> trang đang xây dựng</li>
                      <li>Hỗ trợ đổi màu chủ đạo miễn phí<span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Yinfoc sẽ thao tác thực tiếp trên máy tính của khách hàng thông qua phần mềm Anydesk. sẽ hỗ trợ bạn đổi màu chủ đạo của website miễn phí trong vòng 7 ngày kể từ ngày đăng ký dịch vụ.</p>
                        </span></li>
                      <li>Tối ưu hình ảnh</li>
                      <li>Hỗ trợ qua máy tính từ xa<span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Yinfoc sẽ thao tác thực tiếp trên máy tính của khách hàng thông qua phần mềm Anydesk, TeamViewer.</p>
                        </span></li>
                      <li>Tổng đài <b>24/7</b><span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Tổng đài 0358 186 136 luôn nhận cuộc gọi tiếp nhận yêu cầu hỗ trợ của bạn bất kể ngày nghĩ Lễ, Tết.</p>
                        </span></li>
                    </ul>
                    <ul>
                      <li>Nút liên hệ ngay<span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Mở ứng dụng gọi cho bạn tức thời trên điện thoại thông minh mà không cần phải bấm số</p>
                        </span></li>
                      <li>Tắt/Mở Website<span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Bạn toàn quyền tắt hoặc mở landing page. Hữu ích khi bạn muốn chấm dứt ngay 1 chiến dịch</p>
                        </span></li>
                      <li>Kết nối kênh Google Sheet<span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Dữ liệu từ form sẽ được chuyển thẳng 1 chiều vào tài khoản Google Sheets của bạn</p>
                        </span></li>
                      <li>Nhận thông báo về email khi có đơn hàng </li>
                      <li>Nhúng các mã đo lương hiệu quả: Facebook, Google</li>
                      <li>Form thu thập thông tin</li>
                      <li>Quản lý đơn hàng<span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Cho phép bạn xem, chỉnh sửa, xóa, tìm kiếm các đơn hàng mà khách hàng của bạn đã đặt trên website</p>
                        </span></li>
                    </ul>
                  </div>
                  <div class="package-footer p-3 bg-white">
                    <div class="text--center">
                      <button id="click-package-3" data-package="3" data-dismiss="#modal-package" data-modal="#modal-dk" class="btn btn--radius btn--orange-gradient btn--md">Đặt mua với gói này</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
