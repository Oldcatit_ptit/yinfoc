<div class="modal" id="modal-dk">
  <div class="container">
    <div class="row">
      <div class="col-md-10 offset-md-1">
        <div class="modal-box">
          <div class="modal-inner scroll">
            <div class="row m-0">
              <div class="col-sm-5 p-4">
                @if($data)
                  <div class="prd-card">
                    <div class="prd-card__img bg-img bg-img--43"
                         style="background-image: url({{asset('storage/'.$data->image[0])}})">
                    </div>
                    <div class="p-3 text--center">
                      <p class="text--orange fs--18">Mẫu giao diện: <br> <b class="fs--20">{{$data->name}}</b> </p>
                      <h5 id="info-package" class="mt-4 fs--24">GÓI 3 THÁNG - 1.000.000</h5>
                    </div>
                  </div>
                @endif
              </div>
              <div class="col-sm-7 p-4"
                   style="background: linear-gradient(90deg, #061F37 -8.26%, #663A69 100%); color: #fff">
                <form action="{{url('clientCreateOrder')}}" class="form-basic" method="post">
                  <div class="title mb-0 mt-4">
                    <h5>Quý khách vui lòng cập nhật thông tin để đặt mua:</h5>
                  </div>
                  @csrf
                  <div class="py-3">
                    <input name="package" hidden value="1" id="input-package">
                    <input name="products[]" type="text" value="{{$data->id}}" hidden>
                    <input name="type" type="text" value="1" hidden>
                    <input name="slug" type="text" value="{{$data->slug}}" hidden>
                    <p class="mb-2">Họ và tên *</p>
                    <input class="mb-4 px-3 box-radius" name="name" type="text" required>
                    <p class="mb-2">Số điện thoại *</p>
                    <input class="mb-4 px-3 box-radius" name="phone" type="text" required>
                    <p class="mb-2">Email (không bắt buộc)</p>
                    <input class="mb-4 px-3 box-radius" name="mail" type="text" required>
                    <p class="mb-2">Yêu cầu thêm</p>
                    <textarea class="px-3 mb-0 box-radius" name="des" rows="3" type="text" placeholder="Lời nhắn của ban"></textarea>
                  </div>

                  <div class="d-flex align-items-center px-2">
                    <i class="yinicon-look mr-4"></i>
                    <i>Thông tin người mua sẽ được đảm bảo giữ bí mật</i>
                  </div>
                  <div class="text--center mt-4">
                    <button class="btn btn--md btn--shadow btn--orange-gradient px-5 py-3">Xác nhận</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div class="modal-close"><b><i class="yinicon-x"></i></b></div>
        </div>
      </div>
    </div>
  </div>
</div>
