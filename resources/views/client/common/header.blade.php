<header class="lazyload align-items-end align-items-sm-center"  style="background-image: url('{{asset('storage/load.gif')}}')" data-src="{{url('image/banner.png')}}">
    <div class="c-menu px-3 px-lg-0" id="header-menu">
        <div class="container c-menu__inner">
            <div class="c-menu__logo"><img class="lazyload" data-src="{{url('image/yinfoc.svg')}}" src="{{asset('storage/load.gif')}}" alt=""></div>
            <div class="c-menu__nav">
                <div class="c-menu__nav-btn">
                    <div class="fa fa-bars"></div>
                </div>
                <ul class="radius pd__l15">
                    <li id="menu1">Dịch vụ của Yinfoc</li>
                    <li id="menu2">Sản phẩm của Yinfoc</li>
                    <li id="menu3">Quy trình làm việc</li>
                    <li id="menu4">Cảm nhận khách hàng</li>
                    <li ><a class="c-button c-button--orange radius" data-toggle="modal" href='#c-popup__dk'>Đăng ký tư vấn</a></li>
                </ul>
                <script>
                    $(document).ready(function () {
                        $('#menu1').scrollTo('#yinfoc-service');
                        $('#menu2').scrollTo('#yinfoc-product');
                        $('#menu3').scrollTo('#yinfoc-service');
                        $('#menu4').scrollTo('#work-process');
                    })
                </script>
            </div>
        </div>
    </div>

    <script>
      $('#header-menu').elementFixScroll('c-menu__fix')
    </script>
    <div class="container">
        <div class="row">
            <div class="col-md-4 offset-md-8">
                <div class="d-flex flex-column flex-lg-row mg__b15 header__countdown mb-2 mb-sm-0 pb-3 pb-sm-0">
                    <div class="ml-2 ml-md-0 text-center text-lg-left">Thời gian khuyến mại còn lại: </div>
                    <div class="c-countdown justify-content-center">
                        <div class="c-countdown__item">
                            <span class="dd">00</span>
                            <p>Ngày</p>
                        </div>
                        <div class="c-countdown__item">
                            <span class="hh">00</span>
                            <p>Giờ</p>
                        </div>
                        <div class="c-countdown__item">
                            <span class="mm">00</span>
                            <p>Phút</p>
                        </div>
                        <div class="c-countdown__item">
                            <span class="ss">00</span>
                            <p>Giây</p>
                        </div>
                    </div>
                    <script>
                        $(document).ready(function () {
                            countdown('.c-countdown', 2019, 3, 6, 19);
                        })
                    </script>
                </div>
                <form id="order-top" action="" method="POST" role="form" class="header__form">
                    <div class="c-field-group">
                        <input name="name" type="text" class="c-input" placeholder="Họ và tên...">
                    </div>
                    <div class="c-field-group">
                        <textarea name="note" rows="3" class="c-input" placeholder="Yêu cầu thiết kế..."></textarea>
                    </div>
                    <div class="c-field-group--inline">
                        <input name="phone" type="text" class="c-input" placeholder="Số điện thoại...">
                        <button type="button" class="c-button c-button--orange">Đăng ký</button>
                    </div>
                    <script>
                        $('#order-top button').click(function(){
                            var alert_content = "Cảm ơn quý khách đã quan tâm đến dịch vụ của Yinfoc! Yêu cầu bạn đã được gửi đến Yinfoc. Chúng tôi sẽ liên hệ lại với bạn. Xin cảm ơn!"
                            var sheet_api_url = "https://script.google.com/macros/s/AKfycbzODDL1U8gAPkc7CAZhZP5OvZ_qnSzYKjArmnD5D7QeSV1yBjAO/exec";
                            var status_default = "new";
                            var date = new Date();
                            date = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes();

                            var data = $(this).parent().parent().serialize() + '&date=' + date +'&status=' + status_default;
                            $.ajax({
                                type: 'GET',
                                url: sheet_api_url,
                                dataType: 'json',
                                crossDomain: true,
                                data: data,
                                success: function () {
                                    alert(alert_content);
                                    location.reload();
                                }
                            })
                        })
                    </script>
                </form>
            </div>
        </div>
    </div>
    <div class="header__bottom">
        <div class="container">
            <div class="scroll-animation">
                <div></div>
            </div>
        </div>
    </div>
    <div class="header__after">
        <img class="lazyload" data-src="{{url('image/laptop.png')}}" src="{{asset('storage/load.gif')}}" alt="">
    </div>
</header>
<div class="modal" id="c-popup__dk">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                @include('client.component.formSubmit')
            </div>
        </div>
    </div>
</div>

<script>
    if( $(window).width() < 768 ) {
        $('header').css('background-image', 'url(' + '/image/banner-mb.png' + ')');
    }
</script>
