@if(Session::get('order_status'))
<div class="modal @if(Session::get('order_status')) open @endif" id="modal-success">
  <div class="container">
    <div class="row">
      <div class="col-md-6 offset-md-3">
        <div class="modal-box p-4">
          <div class="modal-inner text--center scroll">
            <img class="w--30" src="img/icon/user-support.svg" alt="">
            <div class="title mt-4 mb-3">
              <h4>Đặt mua giao diện thành công!</h4>
              <p class="title--blue">
                Cảm ơn bạn đã sử dụng dịch vụ của chúng tôi!
              </p>
            </div>
            <div>
              <div class="title mb-0">
                <h5>Vui lòng thanh toán bằng chuyển khoản ngân hàng</h5>
                <p><small>Nội dung chuyển khoản:</small></p>
                <p>
                  <small>
                    [<span class="text--orange">Mã đơn hàng</span>]_
                    [<span class="text--orange">Số điện thoại</span>]_
                    [<span class="text--orange">Họ và tên</span>]
                  </small>
                </p>
                <p><small>Tương ứng với</small></p>
                <p>
                  <small>
                    <span class="text--orange">{{Session::get('order_code')}}</span>_
                    <span class="text--orange">{{Session::get('order_phone')}}</span>_
                    <span class="text--orange">{{Session::get('order_name')}}</span>
                  </small>
                </p>
              </div>
              <div class="row" style="text-align: left">
                <div class="col-sm-6 col-md-12 col-xl-6 mt-3">
                  <div class="w--100 d-flex align-items-center p-2 bg-gray-light">
                    <div>
                      <img style="width: 100px" src="{{url('img/Techcombank.png')}}" alt="">
                    </div>
                    <p class="ml-3" style="font-size: 13px">
                      Chủ TK: {{env('BANK_USER1')}} <br>
                      Chi nhánh: {{env('BANK_ADDRESS1')}} <br>
                      STK: {{env('BANK_NUMBER1')}}
                    </p>
                  </div>
                </div>
                <div class="col-sm-6 col-md-12 col-xl-6 mt-3">
                  <div class="w--100 d-flex align-items-center p-2 bg-gray-light">
                    <div>
                      <img style="width: 100px" src="{{url('img/VPbank.png')}}" alt="">
                    </div>
                    <p class="ml-3" style="font-size: 13px">
                      Chủ TK: {{env('BANK_USER2')}} <br>
                      Chi nhánh: {{env('BANK_ADDRESS2')}} <br>
                      STK: {{env('BANK_NUMBER2')}}
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <p class="mt-3">
            <small>
            Thông tin đơn hàng của bạn đã được gửi về email <br>
            Nhân viên CSKH sẽ sớm gọi cho bạn để tư vấn chi tiết hơn.
            </small>
          </p>
          <div class="modal-close"><b>x</b></div>
        </div>
      </div>
    </div>
  </div>
</div>
@endif
