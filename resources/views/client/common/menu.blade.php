<div class="is-fix is-fix--tl w--100 menu" id="main_menu">
  <div class="container">
    <div class="menu-inner">
      <a href="/"><img class="menu__logo" src="{{url('img/logo.svg')}}" alt=""></a>
      <div class="menu__nav">
        <ul>
          <li @if(\Request::is('gioi-thieu')) class="active" @endif>
            <a href="{{url('gioi-thieu')}}">Giới thiệu</a>
          </li>
          <li @if(\Request::is('kho-giao-dien/*') || \Request::is('kho-giao-dien')) class="active" @endif>
            <a href="{{url('kho-giao-dien')}}">Kho giao diện</a>
          </li>
          <li @if(\Request::is('dich-vu') || \Request::is('dich-vu/*')) class="active" @endif>
            <a href="{{url('dich-vu')}}">Gói dịch vụ</a>
          </li>
          {{--                    <li @if(\Request::is('blog/*') || \Request::is('blog')) class="active" @endif>--}}
          {{--                      <a href="{{url('blog')}}">Blog</a>--}}
          {{--                    </li>--}}
          <li @if(\Request::is('lien-he')) class="active" @endif>
            <a href="{{url('lien-he')}}">Liên hệ</a>
          </li>
        </ul>
      </div>
      <div class="menu__tool">
        @if(!\Request::is('kho-giao-dien/*') && !\Request::is('kho-giao-dien'))
          <a href="tel:0966287678" class="d-flex align-items-center btn btn--orange-gradient">
            <i class="yinicon-call"></i> Gọi tư vấn
          </a>
        @else
          <a href="tel:0966287678" class="d-flex align-items-center btn btn--orange-gradient">
            <i class="yinicon-call"></i> Gọi tư vấn
          </a>
        @endif
      </div>

      <div class="menu__btn">
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
  </div>
</div>
<script>
  $('#main_menu').elementFixScroll('scroll')
</script>
