@include('client.common.head')
<body>
<header>
    @include('client.common.menu')
    @yield('banner')
</header>
</body>

@yield('content')
@include('client.common.footer')
</html>

