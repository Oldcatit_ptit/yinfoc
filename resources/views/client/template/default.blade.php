@include('client.common.head')
<body>
{!! $code->other !!}
<header>
    @include('client.common.menu')
    @yield('banner')
</header>
</body>

@yield('content')
@include('client.common.top-footer')
@include('client.common.footer')
</html>

