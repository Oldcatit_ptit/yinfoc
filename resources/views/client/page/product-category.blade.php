@extends('client.template.none-top-footer')

@section('style')
  <link rel="stylesheet" href="">
@endsection
@section('banner')
  <div class="banner banner-category bg-img" style="background-image: url({{url('img/category_banner.png')}})">
    <div class="bg-img__inner d-flex align-items-center">
      <div class="col-md-6 offset-md-3 text--center">
        <div class="title mt-0 mt-md-5 pt-5">
          <p class="fs--20 mb-4 text--white">HÀNG TRĂM MẪU THIẾT KẾ LANDING PAGE DÀNH CHO TẤT CẢ CÁC LĨNH VỰC</p>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('content')
  <div class="d-flex">
    <div class="category-button">
      @if(isset($category))
        Danh mục: {{$category->name}}
      @else
      Tìm theo danh mục
      @endif
</div>
<div class="category-menu pt-0 scroll scroll-small">
  <div class="category-menu-close">
    <i class="yinicon-x"></i>
  </div>
  <img src="{{url('img/banner-ads-small.jpg')}}" alt="">
  <h4>Danh mục</h4>
  <ul>
    <li class="@if(!isset($category)) active @endif"><a href="{{route('product-category-all')}}">Tất cả</a></li>
    @foreach($categories as $cate)
      <li class="@if(isset($category) && $category->id == $cate->id) active @endif"><a
          href="{{route('product-category-view', ['slug'=>$cate->slug])}}">{{$cate->name}}</a></li>
    @endforeach
  </ul>
</div>
<div class="category-list-pr scroll scroll-small">
  <div class="row">
    @foreach($products as $pr)
      <div class="col-md-3">
        <div data-aos="fade-up" class="prd-card mb-4">
{{--          @if(isset($pr->price_sale))--}}
{{--            <div class="prd-card__sale--square">--}}
{{--              {{ ROUND(($pr->price_sale / $pr->price * 100),0) }}%--}}
{{--            </div>--}}
{{--          @endif--}}
          <div class="prd-card__img bg-img bg-img--43 mb-2"
               style="background-image: url({{asset('storage/'.explode(',', $pr->image)[0])}})">
            <div class="prd-card__tool">
              <div>
                <a href="{{url('landing-page/'.$pr->slug)}}" class="btn btn--white btn--shadow w--100">
                  <i class="yinicon-view" style="margin-left: -10px"></i> Xem giao diện
                </a>
              </div>
            </div>
          </div>
{{--          <div class="prd-card__txt d-flex justify-content-between">--}}
            <div class="prd-card__info">
              <p><b>{{$pr->name}}</b></p>
              <small>Design by Yinfoc</small>
            </div>
{{--            <div class="prd-card__price text--right">--}}
{{--              @if(isset($pr->price_sale))--}}
{{--                <small><strike>{{number_format($pr->price,0,'.', '.')}} vnđ</strike></small>--}}
{{--                <p><b>{{number_format($pr->price_sale,0,'.', '.')}} vnd</b></p>--}}
{{--              @else--}}
{{--                <p><b>{{number_format($pr->price,0,'.', '.')}} vnđ</b></p>--}}
{{--              @endif--}}
{{--            </div>--}}
{{--          </div>--}}
        </div>
      </div>
    @endforeach
  </div>
  {{--<div class="text--center">--}}
  {{--<a class="btn btn--shadow btn--white text--orange">--}}
  {{--Xem tiếp <span class="yinicon-arrow-right"></span>--}}
  {{--</a>--}}
  {{--</div>--}}
</div>
</div>
@endsection
