@extends('client.template.default')

@section('style')
    <link rel="stylesheet" href="">
@endsection
@section('banner')
    <section class="bg-blue-dark text--white">
        <div class="container mt-5 mb-5 pt-5 pb-5">
            <div class="row">
                <div class="col-lg-5">
                    <h1 class="mb-3">Bạn cần hỗ trợ? <br>
                        Hãy gọi cho chúng tôi!</h1>
                    <p class="text--white-support">
                        Chúng tôi luôn sẵn sàng tư vấn, giải đáp mọi thắc mắc của bạn MIỄN PHÍ mọi lúc, mọi nơi. Hãy gọi ngay cho chúng tôi theo hotline <a href="tel:0362519215" class="text--orange">0362519215</a> hoặc để lại thông tin theo form bên dưới chúng tôi sẽ gọi lại.
                    </p>
                </div>
                <div class="col-sm-8 col-lg-4 offset-lg-1 mt-5">
                    <div class="title">
                        <h5>Thông tin thanh toán</h5>
                    </div>
                    <div class="row text--white-support">
                        <div class="col-6">
                            <img src="img/Techcombank.svg" alt="">
                            <p class="mt-3">
                                Chủ TK: Lê Quang Vinh <br>
                                STK: 12345678901
                            </p>
                        </div>
                        <div class="col-6">
                            <img src="img/VPbank.svg" alt="">
                            <p class="mt-3">
                                Chủ TK: Lê Quang Vinh <br>
                                STK: 12345678901
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-md-2 mt-5">
                    <div class="title">
                        <h5>Liên hệ</h5>
                    </div>
                    <p><a class="text--white-support" href="mailto:yinfoc@gmail.com">yinfoc@gmail.com</a></p>
                    <p><a class="text--white-support" href="tel:036.251.9215">036.251.9215</a></p>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('content')
@endsection
