@extends('client.template.none-top-footer')

@section('style')
  <link rel="stylesheet" href="">
@endsection
@section('banner')
  <div class="banner-md banner bg-img bg-blue-dark text--white"
       @if(isset($banner))
       style="background-image: url({{url(asset('storage/'.$banner->image))}})"
       @endif
  >
    <div class="bg-img__inner d-flex align-items-center text--center">
      <div class="container">
        <div class="title mt-0 mt-md-5 pt-5">
          <h1 class="mb-3">Gói dịch vụ YINFOC</h1>
          <p class="fs--20">Chúng tôi có các gói dịch vụ phù hợp với mọi nhu cầu của bạn</p>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('content')
  <section class="section--gay">
    <div class="container">
      @if(isset($data))
        @foreach($data as $item)
          <div id="{{$item->slug}}" class="row box box--shadow-b service p-0 m-0 mb-4">
            <div class="col-md-4 service-intro" style="background-image: url({{asset('storage/'.$item->image)}})">
              <h3 class="mb-4">{{$item->name}}</h3>
              <div>
                {!! $item->des !!}
              </div>
            </div>
            <div class="col-md-8 service-info">
              <div class="service-price mb-2">
                <b class="fs--30 text--orange">{{$item->price}} /</b>
                <span class="fs--24"><b>{{$item->price_des}}</b></span>
              </div>
              <p><b>{{$item->sup}}</b></p>
              <ul class="service-option mt-4">
                <?php $options = explode(PHP_EOL, $item->option);?>
                @foreach($options as $o)
                  <li><i class="yinicon-check-orange text--orange"></i> {{$o}}</li>
                @endforeach
              </ul>
              @if(isset($item->link))
                <a href="{{$item->link}}" class="btn btn--lg btn--orange-gradient btn--shadow">
                  Vào kho website <span class="yinicon-arrow-right"></span>
                </a>
              @else
                <a data-modal="#modal-dk-{{$item->id}}" href="#" class="btn btn--lg btn--orange-gradient btn--shadow">
                  Đăng ký dịch vụ <span class="yinicon-arrow-right"></span>
                </a>
              @endif
            </div>
          </div>
          @include('client.common.popup-order-service')
        @endforeach
      @endif
    </div>
  </section>
  @include('client.common.popup-order-success')

  <section class="bg-gray-support"></section>
@endsection
