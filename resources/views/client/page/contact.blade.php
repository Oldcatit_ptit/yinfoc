@extends('client.template.default')

@section('style')
  <link rel="stylesheet" href="">
@endsection
@section('banner')
  <section class="bg-blue-dark text--white">
    <div class="container mt-5 mb-5 pt-5 pb-5">
      <div class="row">
        <div class="col-lg-5">
          <h1 class="mb-3">Bạn cần hỗ trợ? <br>
            Hãy gọi cho chúng tôi!</h1>
          <p class="text--white-support">
            Chúng tôi luôn sẵn sàng tư vấn, giải đáp mọi thắc mắc của bạn MIỄN PHÍ mọi lúc, mọi nơi. Hãy gọi ngay cho
            chúng tôi theo hotline <a href="tel:0362519215" class="text--orange">{{$companyInfo->phone}}</a> hoặc để lại
            thông tin theo form bên dưới chúng tôi sẽ gọi lại.
          </p>
        </div>
        <div class="col-sm-8 col-lg-4 offset-lg-1 mt-5">
          <div class="title">
            <h5>Thông tin thanh toán</h5>
            <p><small>Nội dung chuyển khoản:</small></p>
            <small>
              [<span class="text--orange">Mã đơn hàng</span>]_
              [<span class="text--orange">Số điện thoại</span>]_
              [<span class="text--orange">Họ và tên</span>]
            </small>
          </div>
          <div class="row text--white-support">
            <div class="col-6">
              <img src="img/Techcombank.svg" alt="">
              <p class="mt-3">
                Chủ TK: {{env('BANK_USER1')}} <br>
                Chi nhánh: {{env('BANK_ADDRESS1')}} <br>
                STK: {{env('BANK_NUMBER1')}}
              </p>
            </div>
            <div class="col-6">
              <img src="img/VPbank.svg" alt="">
              <p class="mt-3">
                Chủ TK: {{env('BANK_USER2')}} <br>
                Chi nhánh: {{env('BANK_ADDRESS2')}} <br>
                STK: {{env('BANK_NUMBER2')}}
              </p>
            </div>
          </div>
        </div>
        <div class="col-sm-4 col-md-2 mt-5">
          <div class="title">
            <h5>Liên hệ</h5>
          </div>
          <p><a class="text--white-support"
                href="mailto:{{$companyInfo->mail_support}}">{{$companyInfo->mail_support}}</a></p>
          <p><a class="text--white-support" href="tel:{{$companyInfo->phone}}">{{$companyInfo->phone}}</a></p>
        </div>
      </div>
    </div>
  </section>
@endsection
@section('content')
@endsection
