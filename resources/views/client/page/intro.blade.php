@extends('client.template.none-top-footer')
@section('style')
    <link rel="stylesheet" href="">
@endsection
@section('banner')
  <section class="bg-blue-dark text--white" style="height: 50vh">
  </section>
@endsection
@section('content')
  <div class="container" style="margin-top: -30rem">
    <h1 class="text--center text--white pb-5">YINFOC - Niềm tin đến từ sự hài lòng!</h1>
    <div class="mb-5 box px-3 px-md-0">
      <div class="row">
        <div class="col-md-10 offset-md-1">
          <p class="mb-2 fs--20 text--blue-support">Giới thiệu</p>
          <h2>{{ $companyInfo->intro_title }}</h2>
          <img class="my-4" src="{{url(asset('storage/'.$companyInfo->image))}}" alt="">
          <p>
            {!! $companyInfo->intro !!}
          </p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-10 offset-md-1">
        <p class="mb-2 fs--20 text--blue-support">Sản phẩm</p>
        <div class="row">
          <div class="col-md-6">
            <h2>Những sản phẩm nổi bật chúng tôi đã làm</h2>
          </div>
          <div class="col-md-6 text--right pt-3">
            <a href="{{ $companyInfo->behance }}" target="_blank">
              <img class="d-inline" style="width: auto" src="img/icon/behance.svg" alt="">
            </a>
            <a href="{{ $companyInfo->dribbble }}" target="_blank">
              <img class="d-inline ml-3" style="width: auto" src="img/icon/dribble.svg" alt="">
            </a>
          </div>
        </div>
        <div class="row py-5">
          @foreach($data as $item)
            <div class="col-6 col-md-4 mb-3">
              <div class="prd-card text--white">
                <div class="prd-card__img bg-img bg-img--43" style="background-image: url({{asset('storage/'.explode(',', $item->image)[0])}})">
                  <div class="prd-card__tool">
                    <div>
                      <a target="_blank" href="{{$item->link}}" class="btn btn--white btn--shadow w--100">
                        <i class="yinicon-view" style="margin-left: -10px"></i> Xem chi tiết
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
@endsection
