@include('client.common.head')
<body>
<div class="web-view">
  <iframe src="{{$data->url_demo}}" id="demo-landing" name="preview-frame" frameborder="0"></iframe>
</div>
<div class="bg-blue-dark text--white tool-view-bar">
  <div class="container d-flex justify-content-between align-items-center py-2">
    <a class="text--white" href="{{url('kho-giao-dien')}}"><i class="yinicon-arrow-left"></i> Kho giao diện</a>
    <div class="tool-view d-flex align-items-center">
      <div data-view="pc" class="px-3 d-flex align-items-center active">
        <img class="mr-2" style="width: 30px" src="{{url('img/icon/desktop.svg')}}" alt=""> <b>Desktop</b>
      </div>
      <div data-view="tab" class="px-3 d-flex align-items-center">
        <img class="mr-2" style="width: 30px" src="{{url('img/icon/tablet.svg')}}" alt=""> <b>Tablet</b>
      </div>
      <div data-view="mb" class="px-3 d-flex align-items-center">
        <img class="mr-2" style="width: 30px" src="{{url('img/icon/mobile.svg')}}" alt=""> <b>Mobile</b>
      </div>
    </div>

    <div class="d-flex">
      <a href="tell:0966287678" class="btn btn--orange-gradient btn--md px-3 mr-2">
        <i class="yinicon-call"></i> Tư vấn
      </a>
      <a id="btn-buy-demo" data-modal="#modal-package" href="#" class="btn btn--orange-gradient btn--md px-3">Đặt mua</a>
    </div>
  </div>
</div>
@include('client.common.popup-order')
@include('client.common.popup_package')
@include('client.common.popup-order-success')
<script>
  $(document).ready(function () {
    var package = 1;
    $('.tool-view *[data-view]').click(function () {
      $('.tool-view *[data-view]').removeClass('active');
      $(this).addClass('active');
      var view = $(this).data('view');
      var elementView = $('.web-view iframe');
      if (view == 'pc') elementView.css({'width': '100%', 'height': '100%'});
      else if (view == 'tab') elementView.css({'width': '768px', 'height': '90%'});
      else if (view == 'mb') elementView.css({'width': '375px', 'height': '667px'});
    });
    $('*[data-package]').click(function () {
      package = $(this).data('package');
      $('#input-package').val(package)
      if (package === 1) {
        $('#info-package').text("GÓI 3 THÁNG - {{number_format(env('PRICE_PACKAGE_2'),0,'.', '.')}} vnđ")
      } else if (package === 2) {
        $('#info-package').text("GÓI 6 THÁNG - {{number_format(env('PRICE_PACKAGE_2'),0,'.', '.')}} vnđ")
      } else if (package === 3) {
        $('#info-package').text("GÓI 12 THÁNG - {{number_format(env('PRICE_PACKAGE_3'),0,'.', '.')}} vnđ")
      }
    });
    if($(window).width() <= 1200 ) {
      $('iframe').load(function() {
        iResize()
      });
      function iResize() {
        console.log(document.getElementById('demo-landing').contentWindow);
        document.getElementById('demo-landing').style.height =
          document.getElementById('demo-landing').contentWindow.document.body.offsetHeight + 'px';
      }
    }
  });
//   $("#demo-landing html").addClass('height_if');

//   $(document).ready(function () {
//     var pageHeight = 0;

//     function findHighestNode(nodesList) {
//         for (var i = nodesList.length - 1; i >= 0; i--) {
//             if (nodesList[i].scrollHeight && nodesList[i].clientHeight) {
//                 var elHeight = Math.max(nodesList[i].scrollHeight, nodesList[i].clientHeight);
//                 pageHeight = Math.max(elHeight, pageHeight);
//             }
//             if (nodesList[i].childNodes.length) findHighestNode(nodesList[i].childNodes);
//         }
//     }
//     findHighestNode(document.documentElement.childNodes);
//      // The entire page height is found
//     console.log('Page height is', pageHeight);
// });
//   $("<iframe>",{
//         load : function(){
//             console.log(this.scrollHeight);
//             alert(this.width+' '+this.scrollHeight);
//         },
//         src  : "{{$data->url_demo}}"
//     });


</script>
</body>
</html>
