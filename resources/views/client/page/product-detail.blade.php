@extends('client.template.none-top-footer')

@section('style')
  <link rel="stylesheet" href="">
@endsection
@section('banner')
  <div class="prd-page__header bg-blue-dark text--white" style="background-image: url({{asset('storage/'.$categories[0]->image)}})">
    <div class="container">
      <div class="row d-flex align-items-end">
        <div class="col-sm-6">
          @if(isset($categories))
            @foreach($categories as $item)
              <a class="btn--sm bg-green btn--radius text--white" href="#">{{$item->name}}</a>
            @endforeach
          @endif
          <h1>{{$data->name}}</h1>
          <small class="fs--20">Design by yinfoc</small>
        </div>
        <div class="col-sm-6 text--right mt-sm-0">
          <div class="mt-5 d-flex justify-content-start justify-content-sm-end" >
            <a class="btn btn--md bg-blue-gr mr-3 px-5" id="btn-demo" target="_blank"
               href="{{route('product-demo', ['slug'=>$data->slug])}}" data-link-moblile="{{$data->url_demo}}">Xem demo</a>
            <a class="btn btn--md bg-orange-gr px-5" href="#buy" id="btn-buy">
              <span>
                Đặt mua
{{--                <br> <small>( {{number_format($data->price)}} vnđ )</small>--}}
              </span>
            </a>
          </div>
{{--          <small>Chia sẻ hoặc copy link</small>--}}
{{--          <div class="share mt-2">--}}
{{--            <a href="#"><img src="{{url('img/icon/facebook.svg')}}" alt=""></a>--}}
{{--            <a href="#"><img src="{{url('img/icon/behance-fill.svg')}}" alt=""></a>--}}
{{--          </div>--}}
        </div>
      </div>
    </div>
  </div>
@endsection
@section('content')
  <div class="order-bar d-none">
    <div>
      <a href="tel:0966287678"><img height="30" src="img/icon/call-fill.svg" alt=""></a>
    </div>
    <a class="btn btn--md bg-blue-light text--white" target="_blank"
       href="{{route('product-demo', ['slug'=>$data->slug])}}">Dùng thử</a>
    <a data-modal="#modal-dk" class="btn btn--md bg-orange text--white" href="#">
      <span>Đặt mua</span>
    </a>
  </div>

  <section class="bg-gray-light">
    <div class="container">
      <div class="row">
        <div class="col-sm-6 mb-4 mb-sm-0">
          <div class="prd-page__img bg-img bg-img--43"
               style="background-image: url({{asset('storage/'.$data->image[0])}})"></div>
        </div>
        <div class="col-sm-6">
          <div class="prd-page__img bg-img bg-img--43"
               style="background-image: url({{asset('storage/'.$data->image[1])}})"></div>
        </div>
      </div>

{{--      <div class="prd-page__content bg-white mt-5 py-5 px-3">--}}
{{--        <div class="row">--}}
{{--          <div class="col-md-10 offset-md-1 col-lg-8 offset-lg-2">--}}
{{--            <h6 class="title--blue title">Giới thiệu</h6>--}}
{{--            <p>--}}
{{--              {!! $data->des !!}--}}
{{--            </p>--}}
{{--            <hr class="my-5">--}}
{{--            <h6 class="title--blue title">Thông tin website</h6>--}}
{{--            <div class="row">--}}
{{--              @foreach( $data->functions as $item)--}}
{{--                <div class="col-6 mb-3">--}}
{{--                  <i class="yinicon-star text--orange"></i> {{$item}}--}}
{{--                </div>--}}
{{--                @endforeach--}}
{{--            </div>--}}
{{--            <div class="mt-5 d-none d-md-flex justify-content-center ">--}}
{{--              <a class="btn btn--md bg-blue-gr mr-3 px-5" target="_blank"--}}
{{--                 href="{{$data->url_demo}}">Xem demo</a>--}}
{{--              <a data-modal="#modal-dk" class="btn btn--md bg-orange-gr px-5" href="#">--}}
{{--                <span>Đặt mua <br> <small>( {{number_format($data->price)}} vnđ )</small></span>--}}
{{--              </a>--}}
{{--            </div>--}}
{{--          </div>--}}
{{--        </div>--}}
{{--    </div>--}}
      <div id="buy" class="mb-5"></div>
        <div  class="row">
          <div class="col-sm-4">
            <div class="package">
              <div class="package-header p-3">
                <p class="text--orange">Mẫu giao diện: <br> {{$data->name}} </p>
                <h4>GÓI 3 THÁNG - {{number_format(env('PRICE_PACKAGE_1'),0,'.', '.')}} vnđ</h4>
              </div>
              <div class="package-body px-3 py-2 bg-white">
                <h5 class="text--orange">Gói cơ bản</h5>
                <ul>
                  <li>
                    <strike>Tích hợp <b>SSL</b></strike>
                    <span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Những website có bảo mật bằng chứng thư số (SSL) sẽ có những biểu tượng https://, được xác thực bởi tổ chức Let's Encrypt, điều đó giúp cho khách hàng tin tưởng doanh nghiệp của bạn hơn.</p>
                        </span>
                  </li>
                  <li>
                    Tốc độ  <b><3s</b>
                    <span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Cam kết website của bạn sẽ có tốc độ hiển thị dưới 3 giây với điều kiện tốc độ internet tại nơi truy cập bình thường.</p>
                        </span>
                  </li>
                  <li><strike>Miễn phí <b>1 banner</b></strike> <span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Một website đẹp cần phải có ít nhất một banner đẹp để thu hút người dùng. Yinfoc luôn hỗ trợ bạn thực hiện tốt việc này.</p>
                        </span></li>
                  <li><b>10 đơn vị</b> nhập liệu lần đầu<span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Tất cả các gói dịch vụ đều được nhập những dữ liệu quan trọng cơ bản như: thông tin liên hệ, địa chỉ, tên website, logo,… Ngoài ra, tùy theo gói dịch vụ mà bạn được hỗ trợ thêm nhập liệu lần đầu như bài giới thiệu, tầm nhìn, tin tức, sản phẩm,…</p>
                        </span></li>
                </ul>
                <ul>
                  <li><strike><b>Miễn phí</b> trang đang xây dựng</strike></li>
                  <li>Hỗ trợ đổi màu chủ đạo miễn phí<span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Yinfoc sẽ thao tác thực tiếp trên máy tính của khách hàng thông qua phần mềm Anydesk. sẽ hỗ trợ bạn đổi màu chủ đạo của website miễn phí trong vòng 7 ngày kể từ ngày đăng ký dịch vụ.</p>
                        </span>
                  </li>
                  <li><strike>Tối ưu hình ảnh</strike></li>
                  <li><strike>Hỗ trợ qua máy tính từ xa</strike><span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Yinfoc sẽ thao tác thực tiếp trên máy tính của khách hàng thông qua phần mềm Anydesk, TeamViewer.</p>
                        </span></li>
                  <li>Tổng đài <b>24/7</b><span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Tổng đài 0358 186 136 luôn nhận cuộc gọi tiếp nhận yêu cầu hỗ trợ của bạn bất kể ngày nghĩ Lễ, Tết.</p>
                        </span></li>
                </ul>
                <ul>
                  <li>Nút liên hệ ngay<span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Mở ứng dụng gọi cho bạn tức thời trên điện thoại thông minh mà không cần phải bấm số</p>
                        </span></li>
                  <li>Tắt/Mở Website<span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Bạn toàn quyền tắt hoặc mở landing page. Hữu ích khi bạn muốn chấm dứt ngay 1 chiến dịch</p>
                        </span></li>
                  <li><strike>Kết nối kênh Google Sheet</strike><span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Dữ liệu từ form sẽ được chuyển thẳng 1 chiều vào tài khoản Google Sheets của bạn</p>
                        </span></li>
                  <li>Nhận thông báo về email khi có đơn hàng </li>
                  <li>Nhúng các mã đo lương hiệu quả: Facebook, Google</li>
                  <li>Form thu thập thông tin</li>
                  <li>Quản lý đơn hàng<span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Cho phép bạn xem, chỉnh sửa, xóa, tìm kiếm các đơn hàng mà khách hàng của bạn đã đặt trên website</p>
                        </span></li>
                </ul>
              </div>
              <div class="package-footer p-3 bg-white">
                <div class="text--center">
                  <button id="click-package-1" data-package="1" data-dismiss="#modal-package" data-modal="#modal-dk" class="btn btn--radius btn--orange-gradient btn--md">Đặt mua với gói này</button>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="package">
              <div class="package-header p-3">
                <p class="text--orange">Mẫu giao diện: <br>{{$data->name}} </p>
                <h4>GÓI 6 THÁNG - {{number_format(env('PRICE_PACKAGE_2'),0,'.', '.')}} vnđ</h4>
              </div>
              <div class="package-body px-3 py-2 bg-white">
                <h5 class="text--orange">Giảm <b>20%</b> cho lần mua tiếp theo</h5>
                <ul>
                  <li>
                    Tích hợp <b>SSL</b>
                    <span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Những website có bảo mật bằng chứng thư số (SSL) sẽ có những biểu tượng https://, được xác thực bởi tổ chức Let's Encrypt, điều đó giúp cho khách hàng tin tưởng doanh nghiệp của bạn hơn.</p>
                        </span>
                  </li>
                  <li>
                    Tốc độ  <b><3s</b>
                    <span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Cam kết website của bạn sẽ có tốc độ hiển thị dưới 3 giây với điều kiện tốc độ internet tại nơi truy cập bình thường.</p>
                        </span>
                  </li>
                  <li>Miễn phí <b>1 banner</b> <span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Một website đẹp cần phải có ít nhất một banner đẹp để thu hút người dùng. Yinfoc luôn hỗ trợ bạn thực hiện tốt việc này.</p>
                        </span></li>
                  <li><b>20 đơn </b> nhập liệu lần đầu <span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Tất cả các gói dịch vụ đều được nhập những dữ liệu quan trọng cơ bản như: thông tin liên hệ, địa chỉ, tên website, logo,… Ngoài ra, tùy theo gói dịch vụ mà bạn được hỗ trợ thêm nhập liệu lần đầu như bài giới thiệu, tầm nhìn, tin tức, sản phẩm,…</p>
                        </span></li>
                </ul>
                <ul>
                  <li><strike><b>Miễn phí</b> trang đang xây dựng</strike></li>
                  <li>Hỗ trợ đổi màu chủ đạo miễn phí<span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Yinfoc sẽ thao tác thực tiếp trên máy tính của khách hàng thông qua phần mềm Anydesk. sẽ hỗ trợ bạn đổi màu chủ đạo của website miễn phí trong vòng 7 ngày kể từ ngày đăng ký dịch vụ.</p>
                        </span></li>
                  <li>Tối ưu hình ảnh</li>
                  <li>Hỗ trợ qua máy tính từ xa<span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Yinfoc sẽ thao tác thực tiếp trên máy tính của khách hàng thông qua phần mềm Anydesk, TeamViewer.</p>
                        </span></li>
                  <li>Tổng đài <b>24/7</b><span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Tổng đài 0358 186 136 luôn nhận cuộc gọi tiếp nhận yêu cầu hỗ trợ của bạn bất kể ngày nghĩ Lễ, Tết.</p>
                        </span></li>
                </ul>
                <ul>
                  <li>Nút liên hệ ngay<span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Mở ứng dụng gọi cho bạn tức thời trên điện thoại thông minh mà không cần phải bấm số</p>
                        </span></li>
                  <li>Tắt/Mở Website<span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Bạn toàn quyền tắt hoặc mở landing page. Hữu ích khi bạn muốn chấm dứt ngay 1 chiến dịch</p>
                        </span></li>
                  <li>Kết nối kênh Google Sheet<span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Dữ liệu từ form sẽ được chuyển thẳng 1 chiều vào tài khoản Google Sheets của bạn</p>
                        </span></li>
                  <li>Nhận thông báo về email khi có đơn hàng </li>
                  <li>Nhúng các mã đo lương hiệu quả: Facebook, Google</li>
                  <li>Form thu thập thông tin</li>
                  <li>Quản lý đơn hàng<span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Cho phép bạn xem, chỉnh sửa, xóa, tìm kiếm các đơn hàng mà khách hàng của bạn đã đặt trên website</p>
                        </span></li>
                </ul>
              </div>
              <div class="package-footer p-3 bg-white">
                <div class="text--center">
                  <button id="click-package-2" data-package="2" data-dismiss="#modal-package" data-modal="#modal-dk" class="btn btn--radius btn--orange-gradient btn--md">Đặt mua với gói này</button>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="package">
              <div class="package-header p-3">
                <p class="text--orange">Mẫu giao diện: <br>{{$data->name}} </p>
                <h4>GÓI 1 NĂM - {{number_format(env('PRICE_PACKAGE_3'),0,'.', '.')}} vnđ</h4>
              </div>
              <div class="package-body px-3 py-2 bg-white">
                <h5 class="text--orange">Giảm <b>30%</b> cho lần mua tiếp theo</h5>
                <ul>
                  <li>
                    Tích hợp <b>SSL</b>
                    <span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Những website có bảo mật bằng chứng thư số (SSL) sẽ có những biểu tượng https://, được xác thực bởi tổ chức Let's Encrypt, điều đó giúp cho khách hàng tin tưởng doanh nghiệp của bạn hơn.</p>
                        </span>
                  </li>
                  <li>
                    Tốc độ  <b><3s</b>
                    <span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Cam kết website của bạn sẽ có tốc độ hiển thị dưới 3 giây với điều kiện tốc độ internet tại nơi truy cập bình thường.</p>
                        </span>
                  </li>
                  <li>Miễn phí <b>1 banner</b> <span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Một website đẹp cần phải có ít nhất một banner đẹp để thu hút người dùng. Yinfoc luôn hỗ trợ bạn thực hiện tốt việc này.</p>
                        </span></li>
                  <li><b>40 đơn vị</b> nhập liệu lần đầu <span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Tất cả các gói dịch vụ đều được nhập những dữ liệu quan trọng cơ bản như: thông tin liên hệ, địa chỉ, tên website, logo,… Ngoài ra, tùy theo gói dịch vụ mà bạn được hỗ trợ thêm nhập liệu lần đầu như bài giới thiệu, tầm nhìn, tin tức, sản phẩm,…</p>
                        </span></li>
                </ul>
                <ul>
                  <li><b>Miễn phí</b> trang đang xây dựng</li>
                  <li>Hỗ trợ đổi màu chủ đạo miễn phí<span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Yinfoc sẽ thao tác thực tiếp trên máy tính của khách hàng thông qua phần mềm Anydesk. sẽ hỗ trợ bạn đổi màu chủ đạo của website miễn phí trong vòng 7 ngày kể từ ngày đăng ký dịch vụ.</p>
                        </span></li>
                  <li>Tối ưu hình ảnh</li>
                  <li>Hỗ trợ qua máy tính từ xa<span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Yinfoc sẽ thao tác thực tiếp trên máy tính của khách hàng thông qua phần mềm Anydesk, TeamViewer.</p>
                        </span></li>
                  <li>Tổng đài <b>24/7</b><span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Tổng đài 0358 186 136 luôn nhận cuộc gọi tiếp nhận yêu cầu hỗ trợ của bạn bất kể ngày nghĩ Lễ, Tết.</p>
                        </span></li>
                </ul>
                <ul>
                  <li>Nút liên hệ ngay<span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Mở ứng dụng gọi cho bạn tức thời trên điện thoại thông minh mà không cần phải bấm số</p>
                        </span></li>
                  <li>Tắt/Mở Website<span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Bạn toàn quyền tắt hoặc mở landing page. Hữu ích khi bạn muốn chấm dứt ngay 1 chiến dịch</p>
                        </span></li>
                  <li>Kết nối kênh Google Sheet<span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Dữ liệu từ form sẽ được chuyển thẳng 1 chiều vào tài khoản Google Sheets của bạn</p>
                        </span></li>
                  <li>Nhận thông báo về email khi có đơn hàng </li>
                  <li>Nhúng các mã đo lương hiệu quả: Facebook, Google</li>
                  <li>Form thu thập thông tin</li>
                  <li>Quản lý đơn hàng<span class="tooltip">
                          <i class="fa fa-question-circle"></i>
                          <p>Cho phép bạn xem, chỉnh sửa, xóa, tìm kiếm các đơn hàng mà khách hàng của bạn đã đặt trên website</p>
                        </span></li>
                </ul>
              </div>
              <div class="package-footer p-3 bg-white">
                <div class="text--center">
                  <button id="click-package-1" data-package="3" data-dismiss="#modal-package" data-modal="#modal-dk" class="btn btn--radius btn--orange-gradient btn--md">Đặt mua với gói này</button>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
  </section>
  @include('client.common.popup-order')
  @include('client.common.popup-order-success')
  <section class="bg-gray-light"></section>
  <script>
    $(document).ready(function () {
      var package = 1;
      $('*[data-package]').click(function () {
        package = $(this).data('package');
        $('#input-package').val(package)
        if (package === 1) {
          $('#info-package').text("GÓI 3 THÁNG - {{number_format(env('PRICE_PACKAGE_2'),0,'.', '.')}} vnđ")
        } else if (package === 2) {
          $('#info-package').text("GÓI 6 THÁNG - {{number_format(env('PRICE_PACKAGE_2'),0,'.', '.')}} vnđ")
        } else if (package === 3) {
          $('#info-package').text("GÓI 12 THÁNG - {{number_format(env('PRICE_PACKAGE_3'),0,'.', '.')}} vnđ")
        }
      });

      if($(window).width() < 1200 ) {
        var link = $('#btn-demo').data('link-moblile')
        $('#btn-demo').attr('href', link)
        $('#btn-demo').attr('target', "")
      }
    });
  </script>
@endsection
