@extends('client.template.none-top-footer')

@section('style')
  <link rel="stylesheet" href="">
@endsection
@section('banner')
  @if(isset($banner))
    <div class="lazyload banner bg-img"
         style="background-image: url('{{asset('img/banner-min.png')}}')"
         data-src="{{url(asset('storage/'.$banner->image))}}"
        >
      <div class="bg-img__inner d-flex align-items-center">
        <div class="col-md-6 offset-md-3 text--center">
          <div class="title mt-0 mt-md-5 pt-5">
            <h1 class="mb-5">{!! $banner->name !!}</h1>
            <p>{!! $banner->des !!}</p>
          </div>
          <a href="{{ $banner->link }}" class="btn btn--lg btn--orange-gradient mt-0 mt-md-5">{{ $banner->link_name }}<span
              class="yinicon-arrow-right"></span></a>
        </div>
      </div>
    </div>
  @endif
@endsection
@section('content')
  @include('client.component.banner-ads')
  @include('client.component.flash-sale', ['data' => $flashSale])
  @include('client.component.why')
  @include('client.component.themes', [ 'data' => $newtheme ])
  @include('client.component.service', [ 'data' => $servicies ])
  @include('client.component.feedback', [ 'data' => $feedback ])
@endsection
