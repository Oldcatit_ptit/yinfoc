@if(isset($data[0]))
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-6 p-0">
        <div class="lazyload bg-img bg-img--32 review-img" style="background-image: url('{{asset('storage/load.gif')}}')" data-src="{{asset('storage/'.$data[0]->image)}}" ></div>
      </div>
      <div class="col-sm-6 bg-gray d-flex text--center align-items-center py-4">
        <div class="w--100">
          <p class="text--center mb-4">Tất cả khách hàng đều hài lòng khi đến với <b>YINFOC</b></p>
          <div class="review-slide">
            @foreach($data as $item)
              <div class="lazyload review-slide__item" data-img="{{asset('storage/'.$item->image)}}">
                <h5>{{$item->content}}</h5>
                <p class="mt-4">
                  {{$item->name}}<br>
                  {{$item->name_sub}}
                </p>
              </div>
            @endforeach
          </div>

          <script>
              $(document).ready(function () {
                  $('.slick-dots>li').click(function () {
                      var linkactive = $('.review-slide__item.slick-active').attr('data-img');
                      $('.review-img').css('background-image', 'url(' + linkactive + ')');
                  })
              })
          </script>
        </div>
      </div>
    </div>
  </div>
  @endif
  </section>
