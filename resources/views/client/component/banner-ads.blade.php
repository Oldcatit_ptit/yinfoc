<section class="pb-0">
  <div class="container">
    <a class="d-none d-sm-block" href="{{url('kho-giao-dien')}}">
      <img src="{{url('img/banner-ads.jpg')}}" alt="">
    </a>
    <a class="d-block d-sm-none" href="{{url('kho-giao-dien')}}">
      <img src="{{url('img/banner-ads-small.jpg')}}" alt="">
    </a>
  </div>
</section>
