<section class="section">
  <div class="container">
    <div class="row">
      <div class="col-md-7">
        <div class="title title--center title--blue">
          <h2>Tại sao nên sử dụng dịch vụ của Yinfoc</h2>
          <p class="fs--20">Mỗi tuần, chúng tôi hỗ trợ cho <b>+100 khách hàng</b> <br>
            sử dụng dịch vụ của YinfoC trên toàn quốc</p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-7 d-flex align-items-center">
        <img class="lazyload" data-src="img/home_intro_illustration.png" src="img/home_intro_illustration-min.png" alt="Tại sao nên sử dụng dịch vụ của Yinfoc">
      </div>
      <div class="col-md-5">
        <div class="intro">
          <div data-aos="fade-up" class="intro-item d-flex align-items-center">
            <div class="intro-item__icon">
              <i class="yinicon-heart"></i>
            </div>
            <div class="intro-item__txt">
              <h4>UY TÍN</h4>
              <p>YINFOC là đơn vị thiết kế website uy tín. 99% dự án nhận được sự đánh giá cao từ đối tác.</p>
            </div>
          </div>
          <div data-aos="fade-up" class="intro-item d-flex align-items-center">
            <div class="intro-item__icon">
              <i class="yinicon-coin"></i>
            </div>
            <div class="intro-item__txt">
              <h4>TIẾT KIỆM CHI PHÍ</h4>
              <p>Nhờ có YINFOC, bạn hoàn toàn có thể sở hữu ngay cho mình một website chuẩn SEO với chi phí rẻ nhất hiện nay.</p>
            </div>
          </div>
          <div data-aos="fade-up" class="intro-item d-flex align-items-center">
            <div class="intro-item__icon">
              <i class="yinicon-time-left"></i>
            </div>
            <div class="intro-item__txt">
              <h4>TỐI ƯU THỜI GIAN</h4>
              <p>YINFOC sở hữu đội ngũ chuyên gia thiết kế website hàng đầu, có thể hoàn thành các website/ landing page cho các doanh nghiệp trong vòng 48 giờ.</p>
            </div>
          </div>
          <div data-aos="fade-up" class="intro-item d-flex align-items-center">
            <div class="intro-item__icon">
              <i class="yinicon-shield"></i>
            </div>
            <div class="intro-item__txt">
              <h4>BẢO HÀNH TRỌN ĐỜI</h4>
              <p>Chúng tôi cam kết bảo hành, sửa chữa cho bạn mọi lúc, mọi nơi khi có phát sinh sự cố từ hệ thống.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
