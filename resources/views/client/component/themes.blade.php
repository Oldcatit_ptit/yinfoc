<section class="section--gay">
  <div class="container">
    <div class="d-flex flex-column flex-sm-row justify-content-start justify-content-sm-between">
      <div class="title title--blue">
        <h2>Kho giao diện Yinfoc</h2>
        <p class="fs--20"><b>100+</b> mẫu Landing Page với đầy đủ các ngành nghề HOT nhất thị trường.</p>
      </div>
      <a href="{{route('product-category-all')}}" class="mt-0 mb-5 mt-sm-5">Xem thêm <i class="yinicon-arrow-right"></i></a>
    </div>
    @if($data)
      <div class="prd-slide row">
        @foreach($data as $item)
        <div class="col-md-4">
          <div data-aos="fade-up" class="prd-card">
{{--            @if(isset($item->price_sale))--}}
{{--              <div class="prd-card__sale--square">--}}
{{--                {{ ROUND(($item->price_sale / $item->price * 100),0) }}%--}}
{{--              </div>--}}
{{--            @endif--}}
            <div class="lazyload prd-card__img bg-img bg-img--43 mb-2"
                 style="background-image: url('{{asset('storage/load.gif')}}')"
                 data-src="{{asset('storage/'.explode(',', $item->image)[0])}}"
                >
              <div class="prd-card__tool">
                <div>
                  <a target="_blank" href="{{route('product-demo', ['slug' => $item->slug])}}" class="btn btn--white btn-adow w--100">
                    <i class="yinicon-view" style="margin-left: -10px"></i> Dùng thử
                  </a>
                </div>
              </div>
            </div>
{{--            <div class="prd-card__txt d-flex justify-content-between">--}}
              <div class="prd-card__info">
                <p><b>{{$item->name}}</b></p>
                <small>Design by Yinfoc</small>
              </div>
{{--              <div class="prd-card__price text--right">--}}
{{--                @if(isset($item->price_sale))--}}
{{--                  <small><strike>{{number_format($item->price,0,'.', '.')}} vnđ</strike></small>--}}
{{--                  <p><b>{{number_format($item->price_sale,0,'.', '.')}} vnd</b></p>--}}
{{--                @else--}}
{{--                  <p><b>{{number_format($item->price,0,'.', '.')}} vnđ</b></p>--}}
{{--                @endif--}}
{{--              </div>--}}
{{--            </div>--}}
          </div>
        </div>
        @endforeach
      </div>
    @endif
  </div>
</section>
