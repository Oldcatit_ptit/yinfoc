<section class="bg-gray-support">
  <div class="container">
    <div class="title title--blue">
      <h2>Các dịch vụ khác của YINFOC</h2>
    </div>
    @if(isset($data))
    <div class="row">
      @foreach($data as $item)
      <div class="col-sm-6 col-md-6">
        <div class="service-card mb-3">
          <div class="lazyload bg-img bg-img--32" style="background-image: url('{{asset('storage/load.gif')}}')" data-src="{{asset('storage/'.$item->image)}}">
          </div>
          <div class="service-card__inner text--center">
            <h4>{{$item->name}}</h4>
            <p>Giá: {{$item->price}}</p>
            <a href="{{route('service-view')}}" class="btn btn--radius bg-white py-2 mt-3">Xem chi tiết</a>
          </div>
        </div>
      </div>
      @endforeach
    </div>
    @endif
  </div>
</section>
