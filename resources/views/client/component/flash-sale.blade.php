@if(isset($flashSale))
  <section class="section">
    <div class="container">
      {{-- <div class="d-flex flex-column flex-sm-row justify-content-between align-items-center countdown_box"> --}}
        {{-- <img class="mb-2 mb-sm-0" src="img/flatsale_text.svg" alt=""> --}}
        {{-- <div class="d-flex align-items-center"> --}}
          {{-- <div class="mr-2 fs--20"> Kết thúc trong </div> --}}
          {{-- <div class="countdown"> --}}
            {{-- <div class="countdown__item"> --}}
              {{-- <div class="countdown__num dd">02</div> --}}
            {{-- </div> --}}
            {{-- <div class="countdown__item"> --}}
              {{-- <div class="countdown__num hh">23</div> --}}
            {{-- </div> --}}
            {{-- <div class="countdown__item"> --}}
              {{-- <div class="countdown__num mm">47</div> --}}
            {{-- </div> --}}
            {{-- <div class="countdown__item"> --}}
              {{-- <div class="countdown__num ss">44</div> --}}
            {{-- </div> --}}
          {{-- </div> --}}
        {{-- </div> --}}
      {{-- </div> --}}
      <div class="title title--blue">
        <h2>Giao diện được mua nhiều</h2>
      </div>
      <div class="bg-white">
        <div class="row">
        @foreach($data as $item)
          <div class="col-6 col-md-3">
            <div data-aos="zoom-out" class="prd-card mb-4">
              {{-- @if(isset($item->price_sale)) --}}
                {{-- <div class="prd-card__sale--square"> --}}
                  {{-- {{ ROUND(($item->price_sale / $item->price * 100),0) }}% --}}
                {{-- </div> --}}
              {{-- @endif --}}
              <div class="lazyload prd-card__img bg-img bg-img--43 mb-2"  style="background-image: url('{{asset('storage/load.gif')}}')" data-src="{{asset('storage/'.explode(',', $item->image)[0])}}" >
                <div class="prd-card__tool">
                  <div>
                    <a href="{{url('landing-page/'.$item->slug)}}" class="btn btn--white btn--shadow w--100">
                      <i class="yinicon-view" style="margin-left: -10px"></i> Xem giao diện
                    </a>
                  </div>
                </div>
              </div>
              {{-- <div class="prd-card__txt d-flex justify-content-between"> --}}
                <div class="prd-card__info">
                  <p><b>{{$item->name}}</b></p>
                  <small>Design by Yinfoc</small>
                </div>
                {{-- <div class="prd-card__price text--right"> --}}
                  {{-- @if(isset($item->price_sale)) --}}
                    {{-- <small><strike>{{number_format($item->price,0,'.', '.')}} vnđ</strike></small> --}}
                    {{-- <p><b>{{number_format($item->price_sale,0,'.', '.')}} vnd</b></p> --}}
                  {{-- @else --}}
                    {{-- <p><b>{{number_format($item->price,0,'.', '.')}} vnđ</b></p> --}}
                  {{-- @endif --}}
                {{-- </div> --}}
              {{-- </div> --}}
            </div>
          </div>
        @endforeach
      </div>
        <div class="text--right">
          <a href="{{url('kho-giao-dien')}}">Xem thêm <i class="yinicon-arrow-right"></i></a>
        </div>
      </div>
    </div>
  </section>
  
@endif

