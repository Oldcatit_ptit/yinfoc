var gulp = require('gulp'),
  gp_concat = require('gulp-concat'),
  uglify = require('gulp-uglify'),
cleanCSS = require('gulp-clean-css');

gulp.task('minify', function () {
  gulp.src([
    'public/css/aos.css',
    'public/css/slick.min.css',
    'public/css/font.css',
    'public/css/mainold.css',
    'public/css/main.css',
    'public/css/costom.css'
  ]).pipe(gp_concat('combine.css'))
    .pipe(cleanCSS())
    .pipe(gulp.dest('public/css'));
  gulp.src([
    'public/js/jquery2.2.4.min.js',
    'public/js/slick.min.js',
    'public/js/aos.js',
    'public/js/jquery.lazy.min.js',
    'public/js/main.js',
  ]).pipe(gp_concat('combine.js'))
    .pipe(uglify())
    .pipe(gulp.dest('public/js'));
});
