<?php

namespace App\Providers;

use App\CompanyInfo;
use App\User;
use Hash;

use App\IntegratedCode;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    public function register()
    {

    }
    public function boot()
    {
      $companyInfo = CompanyInfo::all()->first();
      $code = IntegratedCode::all()->first();
      View::share('companyInfo', $companyInfo );
      View::share('code', $code );
      Validator::extend('check_old_pass',function ($attributes,$value,$parameter,$validator){
        return Hash::check($value, Auth::user()->password);
        });

        Validator::extend('check_password',function ($attributes,$value,$parameter,$validator){
            $user = User::where('email',request()->email)->first();
            if ($user) {
                return Hash::check($value, $user->password);
            }else{
               return false;
            }
        });
        Validator::extend('check_email',function ($attributes,$value,$parameter,$validator){
            $user = User::whereNotIn('id',[Auth::id()])->where('email',request()->email)->first();
            if ($user) {
                return false;
            }else{
               return true;
            }
        });
    }
}
