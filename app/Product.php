<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = [
        'name',
        'des',
        'image',
        'price',
        'price_sale',
        'url_demo',
        'slug',
        'functions'
    ];

    public function categories(){
        return $this->belongsToMany('App\ProductCategory', 'product_by_categories', 'products_id', 'product_categories_id');
    }

    public function categoriesId(){
        $c = $this->belongsToMany('App\ProductCategory', 'product_by_categories', 'products_id', 'product_categories_id')->get();
        $c_id = [];
        foreach ( $c as $item ) {
            array_push($c_id, $item->id);
        }
        return $c_id;
    }
}
