<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IntegratedCode extends Model
{
    public $timestamps = false;
    protected $table = 'integrated_codes';
    protected $fillable = [
      'chatbot',
      'facebook',
      'google',
      'youtube',
      'other',
    ];
}
