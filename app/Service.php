<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = 'services';
    protected $fillable = [
      'image',
      'name',
      'des',
      'price',
      'price_des',
      'sup',
      'option',
      'link',
      'slug'
    ];
}
