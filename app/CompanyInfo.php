<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyInfo extends Model
{
    public $timestamps = false;
    protected $table = 'company_infos';
    protected $fillable = [
      'mail_support',
      'mail_order',
      'mail_recruitment',
      'phone',
      'phone_tech',
      'address',
      'facebook',
      'google',
      'instagram',
      'youtube',
      'behance',
      'dribbble',
      'name',
      'intro',
      'intro_title',
      'image',
    ];
}
