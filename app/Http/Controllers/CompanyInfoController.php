<?php

namespace App\Http\Controllers;

use App\CompanyInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CompanyInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company_info = DB::table('company_infos')->first();
        return view('admin.company.index')->with('data', $company_info);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }
    public function show(CompanyInfo $companyInfo)
    {
        //
    }

    public function edit(CompanyInfo $companyInfo)
    {
        //
    }

    public function update(Request $request, CompanyInfo $companyInfo)
    {
      $data = $request->all();
      $data['image'] = $companyInfo['image'];
      if ($request->hasfile('image')) {
        $image = $request->file('image');
        $extension = $image->getClientOriginalExtension();
        $file_name = "company_info/" . '_' . date("d_m_Y",time()) . '.' . $extension;
        $image->storeAs('public', $file_name);
        $data['image'] = $file_name;
      }
      $companyInfo->fill($data);
      $companyInfo->save();
      return redirect('yinadmin/company-info');
    }
}
