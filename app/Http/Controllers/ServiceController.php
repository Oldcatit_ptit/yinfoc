<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ServiceController extends Controller
{
  public function index()
  {
    $services = DB::table('services')->get();
    return view('admin.service.index')->with('data', $services);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  public function store(Request $request)
  {
    $image = $request->file('image');
    $image->storeAs('public', $image->getClientOriginalName());
    $data = $request->all();
    $data['image'] = $image->getClientOriginalName();
    $service = new Service();
    $service->fill($data);
    $service->save();
    return redirect('yinadmin/services');
  }

  public function show(Service $service)
  {
    //
  }

  public function edit(Service $service)
  {
    $data = $service;
    return view('admin.service.update', ['data' => $data]);
  }

  public function update(Request $request, Service $service)
  {
    $data = $request->all();
    if (isset($data['image'])) {
      $image = $request->file('image');
      $image->storeAs('public', $image->getClientOriginalName());
      $data['image'] = $image->getClientOriginalName();
    }
    else $data['image'] = $service['image'];
    $service->fill($data);
    $service->save();
    return redirect('yinadmin/services/' . $service['id'] . '/edit');
  }
  public function destroy(Service $service)
  {
    $service->delete();
    return redirect('yinadmin/services');
  }
}
