<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductByCategory;
use Illuminate\Http\Request;

class ProductByCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('product')->get();
        return view('admin.product.category.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductByCategory  $productByCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ProductByCategory $productByCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductByCategory  $productByCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductByCategory $productByCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductByCategory  $productByCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductByCategory $productByCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductByCategory  $productByCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductByCategory $productByCategory)
    {
        //
    }
}
