<?php

namespace App\Http\Controllers;

use App\IntegratedCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IntegratedCodeController extends Controller
{

  public function index()
  {
    $integrated_code = DB::table('integrated_codes')->first();
    return view('admin.integrated_code.index')->with('data', $integrated_code);
  }

  public function create()
  {
    //
  }

  public function store(Request $request)
  {
    //
  }

  public function show(IntegratedCode $integratedCode)
  {
    //
  }


  public function edit(IntegratedCode $integratedCode)
  {
    //
  }


  public function update(Request $request, IntegratedCode $integratedCode)
  {
    $data = $request->all();
    $integratedCode->fill($data);
    $integratedCode->save();
    return redirect('yinadmin/integratedCode');
  }
}

