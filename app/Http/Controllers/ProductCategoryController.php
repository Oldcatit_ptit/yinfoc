<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ProductCategoryController extends Controller
{

  public function index()
  {
    $data = DB::table('product_categories')->get();
    return view('admin.product.category.index', ['data' => $data]);
  }

  public function create()
  {

  }

  public function store(Request $request)
  {
    $file_name = $request->file('image')->getClientOriginalName();
    $category = new ProductCategory();
    $data = $request->all();
    $data['image'] = $file_name;
    $request->file('image')->storeAs('public', $file_name);
    $category->fill($data);
    $category->save();
    return redirect('yinadmin/product-categories');
  }

  public function show($slug)
  {
    $category = ProductCategory::where('slug', $slug)->get()[0];
    $products = $category->products;
    $categories = ProductCategory::all();
    return view('client.page.product-category')
      ->with([
        'category' => $category,
        'categories' => $categories,
        'products' => $products,
      ]);
  }

  public function viewAll() {
    $products = Product::all();
    $categories = ProductCategory::all();
    return view('client.page.product-category')
      ->with([
        'categories' => $categories,
        'products' => $products,
      ]);
  }

  public function edit(ProductCategory $productCategory)
  {
    return view('admin.product.category.update', ['data' => $productCategory]);
  }

  public function update(Request $request, ProductCategory $productCategory)
  {
    $data = $request->all();
    if (!isset($data['image'])) {
      $data['image'] = $productCategory['image'];
    } else {
      $file_name = $request->file('image')->getClientOriginalName();
      $request->file('image')->storeAs('public', $file_name);
      $data['image'] = $file_name;
    };
    $productCategory->fill($data);
    $productCategory->save();
    return redirect('yinadmin/product-categories/' . $productCategory['id'] . '/edit');
  }

  public function destroy(ProductCategory $productCategory)
  {
    $productCategory->delete();
    return redirect('yinadmin/product-categories');
  }
}
