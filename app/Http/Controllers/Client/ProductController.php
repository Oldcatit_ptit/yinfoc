<?php

namespace App\Http\Controllers\Client;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
  public function index($slug)
  {
    $product = Product::where('slug', $slug)->first();
    $product->image = explode(',', $product->image);
    $product->functions = explode('|', $product->functions);
    $category = $product->categories()->get();
    return view('client.page.product-detail')->with(['data' => $product, 'categories' => $category]);
  }

  public function demo($slug)
  {
    $product = Product::where('slug', $slug)->first();
    $product->image = explode(',', $product->image);
    $product->functions = explode('|', $product->functions);
    return view('client.page.product-demo')->with(['data' => $product]);
  }
}
