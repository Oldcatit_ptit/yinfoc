<?php

namespace App\Http\Controllers\Client;

use App\Feedback;
use App\Order;
use App\Product;
use App\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ClientController extends Controller
{
  public function showIntro()
  {
    $product = DB::table('components')->where('type','product')->get();
    return view('client.page.intro')->with('data', $product);
  }
}
