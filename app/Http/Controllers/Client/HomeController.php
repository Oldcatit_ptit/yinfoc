<?php

namespace App\Http\Controllers\Client;

use App\Feedback;
use App\Product;
use App\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
  public function index()
  {
    $newTheme = DB::table('products')->limit(8)->get();
//    $newTheme = DB::table('products')->orderBy('created_at','desc')->limit(8)->get();
    $servicies = Service::all();
    $feedback = Feedback::all();
    $banner = DB::table('components')->where('type', 'banner')->first();
    $flashSale = DB::table('products')->where('price_sale', '!=', null)->limit(8)->get();
    return view('client.page.home')->with([
      'newtheme' => $newTheme,
      'servicies' => $servicies,
      'flashSale' => $flashSale,
      'feedback' => $feedback,
      'banner' => $banner,
    ]);
  }
}
