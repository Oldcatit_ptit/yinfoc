<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ServiceController extends Controller
{
    public function index(){
      $data = DB::table('services')->get();
      $banner = DB::table('components')->where('type', 'banner')->first();
      return view('client.page.servicies')->with([
        'data' => $data,
        'banner' => $banner,
      ]);
    }
}
