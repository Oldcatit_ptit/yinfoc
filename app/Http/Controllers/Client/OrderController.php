<?php

namespace App\Http\Controllers\Client;

use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
  public function clientCreateOrder(Request $request)
  {
    $order = new Order();
    $data = $request->all();
    $order->fill($data);
    $order['code'] = strtoupper(str_random(5));
    $order->save();
    $order['code'] = $order['code'] . $order['id'];
    $order->save();
    $order->products()->sync($data['products']);
    $product = $order->products()->get()[0];
    $this->sendMailAdmin($order, $product);
    $this->sendMailUser($order, $product);
    // $this->addToSheet();
    if ($data['type'] == '1')
      return redirect()->route('product-view', ['slug' => $data['slug']])->with([
        'order_status' => true,
        'order_code' => $order['code'],
        'order_phone' => $order['phone'],
        'order_name' => $order['name'],
      ]);
    return redirect()->route('service-view')->with([
      'order_status' => true,
      'order_code' => $order['code'],
      'order_phone' => $order['phone'],
      'order_name' => $order['name'],
    ]);
  }

  function sendMailAdmin($order, $product)
  {

    \Mail::send('mail.sendAdmin.OrderInfo', ['order' => $order, 'product' => $product],
      function ($message) {
        $message->to('Yinfoclandingpage@gmail.com', 'Visitor')
          ->subject('Yinfoc order')
          ->cc('quangvinhptitcdit@gmail.com', 'Visitor')
          ->cc('ductrinh2927@gmail.com', 'Visitor');
      }
    );
  }
  function sendMailUser($order, $product)
  {

    \Mail::send('mail.sendAdmin.MailOrderUser', ['order' => $order, 'product' => $product],
      function ($message) use ($order, $product){
        $message->to($order['mail'], $order['name'])
          ->subject('Yinfoc order');
      }
    );
  }

  function addToSheet()
  {
      $client = new \Google_Service_Sheets();
  }

  function addToAdmin()
  {

  }
}
