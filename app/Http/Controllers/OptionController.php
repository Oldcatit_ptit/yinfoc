<?php

namespace App\Http\Controllers;

use App\Option;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OptionController extends Controller
{

  public function index()
  {
    $data = DB::table('options')->get();
    return view('admin.product.option.index')->with('data', $data);
  }

  public function create()
  {
    //
  }

  public function store(Request $request)
  {
    $data = $request->all();
    $option = new Option();
    $option->fill($data);
    $option->save();
    return redirect('yinadmin/options');
  }

  public function show(Option $option)
  {
    //
  }

  public function edit(Option $option)
  {
    return view('admin.product.option.update')->with('data', $option);
  }

  public function update(Request $request, Option $option)
  {
    $data = $request->all();
    $option->fill($data);
    $option->save();
    return redirect('yinadmin/options/'.$option['id'].'/edit');
  }

  public function destroy(Option $option)
  {
    $option->delete();
    return redirect('yinadmin/options');
  }
}
