<?php

namespace App\Http\Controllers;

use App\Feedback;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
  public function index()
  {
    $feedback = Feedback::all();
    return view('admin.feedback.index')->with('data', $feedback);
  }

  public function create()
  {
    //
  }

  public function store(Request $request)
  {
    $image = $request->file('image');
    $image->storeAs('public', $image->getClientOriginalName());
    $data = $request->all();
    $data['image'] = $image->getClientOriginalName();
    $feedback = new Feedback();
    $feedback->fill($data);
    $feedback->save();
    return redirect('yinadmin/feedback');
  }

  public function show(Feedback $feedback)
  {

  }

  public function edit(Feedback $feedback)
  {
    return view('admin.feedback.update', ['data' => $feedback]);
  }


  public function update(Request $request, Feedback $feedback)
  {
    $data = $request->all();
    if (isset($data['image'])) {
      $image = $request->file('image');
      $image->storeAs('public', $image->getClientOriginalName());
      $data['image'] = $image->getClientOriginalName();
    }
    else $data['image'] = $feedback['image'];
    $feedback->fill($data);
    $feedback->save();
    return redirect('yinadmin/feedback/' . $feedback['id'] . '/edit');
  }

  public function destroy(Feedback $feedback)
  {
    $feedback->delete();
    return redirect('yinadmin/services');
  }
}
