<?php

namespace App\Http\Controllers;

use App\Component;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ComponentController extends Controller
{
  public function index() {

  }

  public function getFeaturedProducts()
  {
    $featured_products = DB::table('components')->where('type', 'product')->get();
    return view('admin.component.product')->with('data', $featured_products);
  }

  public function getBanners()
  {
    $banners = DB::table('components')->where('type', 'banner')->get();
    return view('admin.component.banner')->with('data', $banners);
  }


  public function create()
  {
    //
  }

  public function store(Request $request)
  {
    $image = $request->file('image');
    $image->storeAs('public', $image->getClientOriginalName());
    $data = $request->all();
    $data['image'] = $image->getClientOriginalName();
    $component_prd = new Component();
    $component_prd->fill($data);
    $component_prd->save();
    if($data['type'] == 'product') return redirect('yinadmin/component/products');
    return redirect('yinadmin/component/banners');
  }


  public function show(Component $component)
  {
    //
  }

  public function edit(Component $component)
  {
    return view('admin.component.update', ['data' => $component]);
  }
  public function update(Request $request, Component $component)
  {
    $data = $request->all();
    if (isset($data['image'])) {
      $image = $request->file('image');
      $image->storeAs('public', $image->getClientOriginalName());
      $data['image'] = $image->getClientOriginalName();
    }
    else $data['image'] = $component['image'];
    $component->fill($data);
    $component->save();
    return redirect('yinadmin/components/' . $component['id'] . '/edit');
  }
  public function destroy(Component $component)
  {
    $component->delete();
    return redirect('yinadmin/component/products');
  }
}
