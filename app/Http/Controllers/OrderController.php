<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
  public function index()
  {

  }

  public function products()
  {
    $orders_prd = Order::where('type', 1)->orderBy('created_at','DESC');
    $orders_prd = $orders_prd->with('services')->get();
    return view('admin.order.product')->with('data',$orders_prd);
  }

  public function service()
  {
    $orders_service = Order::where('type', 2);
    $orders_service = $orders_service->with('products')->get();
    return view('admin.order.service')->with('data',$orders_service);
  }

  public function create()
  {
    //
  }

  public function show(Order $order)
  {
    //
  }

  public function edit(Order $order)
  {
    //
  }

  public function update(Request $request, Order $order)
  {
    //
  }

  public function destroy(Order $order)
  {
    //
  }
}
