<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $categories = DB::table('product_categories')->get();
    $products = DB::table('products')->get();
    $options = DB::table('options')->get();
    $data = [
      'categories' => $categories,
      'products' => $products,
      'options' => $options,
    ];
    return view('admin.product.index', ['data' => $data]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {

  }

  public function store(Request $request)
  {
    $image1 = $request->file('image1')->getClientOriginalName();
    $image2 = $request->file('image2')->getClientOriginalName();
    $productCategory = explode(',', $request->all()['productCategory']);
    $product = new Product();
    $data = $request->all();
    $arr_img = [$image1, $image2];
    $data['image'] = implode(',', $arr_img);
    $data['functions'] = implode('|', $data['functions']);
    $product->fill($data);
    $product->save();
    $request->file('image1')->storeAs('public', $image1);
    $request->file('image2')->storeAs('public', $image2);
    $product->categories()->sync($productCategory);
    return redirect('yinadmin/products');
  }

  public function show($id)
  {
    return view('admin.product.update');
  }

  public function edit(Product $product)
  {
    $categories = DB::table('product_categories')->get();
    $productCate = $product->categoriesId();
    $options = DB::table('options')->get();
    $data = [
      'categories' => $categories,
      'product' => $product,
      'productCate' => $productCate,
      'options' => $options
    ];
    return view('admin.product.update', ['data' => $data]);
  }

  public function update(Request $request, Product $product)
  {
    $arr_img = explode(',', $product['image']);
    $data = $request->all();
    if (isset($data['image1'])) {
      $file_name = $request->file('image1')->getClientOriginalName();
      $request->file('image1')->storeAs('public', $file_name);
      $arr_img[0] = $file_name;
    }
    if (isset($data['image2'])) {
      $file_name = $request->file('image2')->getClientOriginalName();
      $request->file('image2')->storeAs('public', $file_name);
      $arr_img[1] = $file_name;
    }
    $productCategory = explode(',', $request->all()['productCategory']);
    $data['image'] = implode(',', $arr_img);
    $data['functions'] = implode('|', $data['functions']);
    $product->fill($data);
    $product->save();
    $product->categories()->sync($productCategory);
    return redirect('yinadmin/products/' . $product['id'] . '/edit');
  }

  public function destroy(Product $product)
  {
    $product->delete();
    return redirect('yinadmin/products');
  }
}
