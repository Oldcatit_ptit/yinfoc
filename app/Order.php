<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
  protected $table = 'orders';
  protected $fillable = [
    'name',
    'code',
    'des',
    'phone',
    'mail',
    'address',
    'type',
    'package'
  ];

  public function products()
  {
    return $this->belongsToMany(
      'App\Product', 'order_details', 'order_id', 'product_id');
  }

  public function services()
  {
    return $this->belongsToMany('App\Service',
      'order_details', 'order_id', 'product_id');
  }
}
