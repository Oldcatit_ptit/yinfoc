<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $table = 'product_categories';

    protected $fillable = [
        'name',
        'des',
        'image',
        'slug'
    ];

    public function products(){
        return $this->belongsToMany('App\Product', 'product_by_categories', 'product_categories_id', 'products_id');
    }
}
